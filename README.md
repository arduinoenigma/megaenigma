# MegaEnigma

A full featured Enigma Machine using an ATmega 2560 based Arduino Pro Mini and a custom PCB.
https://hackaday.io/project/165559-at-mega-enigma-a-2560-pro-mini-enigma-simulator

Main file is [MegaEnigma.ino](https://gitlab.com/arduinoenigma/megaenigma/blob/master/MegaEnigma.ino)