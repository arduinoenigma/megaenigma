// UniversalEnigma.ino
// @arduinoenigma 2019
//
// a universal enigma machine simulator engine
//
// enigma wiring information obtained from Daniel Palloks Universal Enigma
// http://people.physik.hu-berlin.de/~palloks/js/enigma/index_en.html
// http://people.physik.hu-berlin.de/~palloks/js/enigma/enigma-u_v25_en.html
//
// some information was also obtained from the excellent Crypto Museum website
// https://www.cryptomuseum.com/crypto/enigma/
//
// developing for an arduino requires us to be careful with RAM usage
// as such, the algorithms and enigma data storage here are original work and not copies of Palloks Enigma
//
// mem (with GUI): 19,044 code / 1,049 ram
// max memory:
// uno/nano: 30k code / 2k ram
// mega: 253K code / 8k ram

// The GetCurrentWheel function sets the "etw", "ukw", "currentwheel" and "currentstepnotches" variables
// the enigma engine is responsible for copying "currentwheel" to "wheelX" and "currentstepnotches" to "stepX"
// we need a way to disable rotor4/step4 for three wheel machines

// todo:

// -test maxwheels, maxukw, max rotor logic
// -uses wheel and rotor in function names, unify
// -SetStep function should be isLeverStepping
// -needs an isthislegal() function that will return whether the selected rotor combo is valid or not

// done:
// -set etw returns the maximum number of machines supported (14)
// -step function for lever machines
// -step function for gear machines
// -state machine for enigma encoding
// -get and set set functions for wheel position, ring (all members of structure)
// -implement hasPlugboard(byte pmachine) function returne 1 or 0
// -enigma state machine skips 4th rotor and plugboard if not applicable

const __FlashStringHelper *etw;     /*xls*/

const __FlashStringHelper *currentwheel;     /*xls*/

// 1 2 3 B G
char currentwheelname;     /*xls*/

const __FlashStringHelper *currentstepnotches;     /*xls*/

//Get ENIGMADATASIZE with Serial.println(sizeof(enigmaData_t));
#define ENIGMADATASIZE sizeof(univEnigmaData_t)
#define EEPROMADD 288
#define EEPROMADD1 (EEPROMADD + ENIGMADATASIZE)

struct univEnigmaData_t     /*xls*/
{
  byte Init1;
  unsigned char Ver1;                                                    // Get or Set methods
  byte SerialFunction;    // 0,1,2                                       // G S
  byte Brightness;        // 0,1,2,3,4                                   // G S

  byte MachineID;         // Machine+UKW ID                              // G S

  byte MachineType;                                                      // G S
  byte UKWType;                                                          // G S

  byte RotorType[4];      // 0:left 1:middle 2:right 3:extra wheel       // G S
  byte RingSetting[4];                                                   // G S
  byte RotorPosition[4];                                                 // G S

  //storage space for 4 programmable stepping rotors
  //it has a maximum of 26 bytes of data per wheel, if the string is shorter it copies a 0 to indicate the end
  byte StepPoints[104]; // 0..103 0..25 26..51 52..77 78..103

  byte UKWD[26];

  byte PlugPairs[26];
  byte UHR;

  unsigned char Ver2;
  byte Init2;
}
UnivEnigmaData;

// the enigma rotor pack, enter at etw and go down
// turn around at ukw and come back up, exit through etw
// ukwD is reconfigurable and has to be in RAM, perhaps copy readonly ukw to RAM array
// also see https://www.cryptomuseum.com/crypto/enigma/lf/index.htm for a I wheel with programmable stepping notches
struct univEnigmaWheels_t     /*xls*/
{
  //                                                                        Get or Set methods
  byte LeverStepping;
  byte HasUKWD;                                                          // G
  byte SettableUWK;                                                      // G
  byte Plugboard;
  byte NumberOfWheels;                                                   // G
  byte MaxWheels;                                                        // G
  byte PrintGroups;                                                      // G

  byte NeedsInit;
  byte RotorsChanged;                                                    // G
  byte NeedsSaving;
  byte InitUKW;

  char RotorName[4];                                                     // G

  const __FlashStringHelper *ETW;

  const __FlashStringHelper *Wheels[4];

  // since UKWD is dynamic, this needs to be copied to UnivEnigmaData.UKWD in RAM
  const __FlashStringHelper *UKW;
  const __FlashStringHelper *UKWName;

  // for lever machines, is this wheel turn to step? <<right> 0,1,2,3 <left>
  byte wheellockeddec[4];
  byte wheellockedinc[4];

  byte UseSWPlugs;
  byte UHRPairsOk;                                                      // G
  byte EffectivePlugs[26];
}
UnivEnigmaWheels;


//
//BUG: copy this function to check Enigma.UKWD[] in EnigmaGUI
void dbgCheckRotor(const __FlashStringHelper *tRotor)     /*xls*/
{
  //const PROGMEM char *p = (const char PROGMEM *)tRotor;
  const char *p = (const char *)tRotor;

  byte i = 0;
  char k = 0;

  byte contacts[26];

  byte error = 0;

  for (i = 0; i < 26; i++)
  {
    contacts[i] = 0;
  }
  i = 0;

  do
  {
    k = pgm_read_byte(p + i);
    i++;

    if (k > 0)
    {
      if ((k >= 'a') && (k <= 'z'))
      {
        ++contacts[k - 'a'];
      }
    }
  } while (k != 0);

  for (i = 0; i < 26; i++)
  {
    if (contacts[i] != 1)
    {
      error++;
    }
    Serial.print(contacts[i]);
  }
  Serial.print(F("\x0d\x0a"));

  if (error)
  {
    Serial.println(F("dbgCheckRotor: bad rotor"));
  }
  Serial.print(F("\x0d\x0a"));
}


//
void dbgPrintPROGMEM(const __FlashStringHelper * toPrint)     /*xls*/
{
  //const PROGMEM char *p = (const char PROGMEM *)toPrint;
  const char *p = (const char *)toPrint;

  byte i = 0;
  char k = 0;

  do
  {
    k = pgm_read_byte(p + i);
    i++;

    if ((k < 'a') || (k > 'z'))
    {
      Serial.print(F(" "));
      Serial.print((byte)k);
      Serial.print(F(" "));
    }
    else
    {
      Serial.print((char)(k ^ 32));
    }
  } while (k != 0);

  Serial.print(i);
  Serial.print(F("\x0d\x0a"));
}


//
void dbgPrintEnigma()     /*xls*/
{
  Serial.print(F("Init1: "));
  Serial.println(UnivEnigmaData.Init1);
  Serial.print(F("Init2: "));
  Serial.println(UnivEnigmaData.Init2);
  Serial.print(F("Ver1: "));
  Serial.println(UnivEnigmaData.Ver1);
  Serial.print(F("Ver2: "));
  Serial.println(UnivEnigmaData.Ver2);
  Serial.print(F("SerialFunction: "));
  Serial.println(UnivEnigmaData.SerialFunction);
  Serial.print(F("MachineID: "));
  Serial.println(UnivEnigmaData.MachineID);
  Serial.print(F("MachineType: "));
  Serial.println(UnivEnigmaData.MachineType);
  Serial.print(F("UKWType: "));
  Serial.println(UnivEnigmaData.UKWType);

  for (byte i = 0; i < 4; i++)
  {
    Serial.print(F("RotorType["));
    Serial.print(i);
    Serial.print(F("]: "));
    Serial.println(UnivEnigmaData.RotorType[i]);
  }

  for (byte i = 0; i < 4; i++)
  {
    Serial.print(F("RingSetting["));
    Serial.print(i);
    Serial.print(F("]: "));
    Serial.println((byte)UnivEnigmaData.RingSetting[i]);
  }

  for (byte i = 0; i < 4; i++)
  {
    Serial.print(F("RotorPosition["));
    Serial.print(i);
    Serial.print(F("]: "));
    Serial.println((char)UnivEnigmaData.RotorPosition[i]);
  }

  Serial.print(F("UnivEnigmaWheels.LeverStepping: "));
  Serial.println(UnivEnigmaWheels.LeverStepping);

  Serial.print(F("UnivEnigmaWheels.ETW: "));
  Serial.println(UnivEnigmaWheels.ETW);

  for (byte i = 0; i < 4; i++)
  {
    Serial.print(F("UnivEnigmaWheels.RotorName["));
    Serial.print(i);
    Serial.print(F("]: "));
    Serial.println((char)UnivEnigmaWheels.RotorName[i]);

    Serial.print(F("UnivEnigmaWheels.Wheels["));
    Serial.print(i);
    Serial.print(F("]: "));
    Serial.println(UnivEnigmaWheels.Wheels[i]);

    Serial.print(F("UnivEnigmaData.StepPoints["));
    Serial.print(i);
    Serial.print(F("]: "));

    for (byte j = 0; j < 26; j++)
    {
      Serial.print((char)UnivEnigmaData.StepPoints[i * 26 + j]);
    }
    Serial.print(F("\x0d\x0a"));
  }

  Serial.print(F("UnivEnigmaWheels.UKWName: "));
  Serial.println(UnivEnigmaWheels.UKWName);

  Serial.print(F("UnivEnigmaData.UKW: "));

  for (byte j = 0; j < 26; j++)
  {
    Serial.print((char)UnivEnigmaData.UKWD[j]);
  }
  Serial.print(F("\x0d\x0a"));

  Serial.print(F("UnivEnigmaData.PlugPairs[]: "));

  for (byte j = 0; j < 26; j++)
  {
    Serial.print(UnivEnigmaData.PlugPairs[j]);
    Serial.print(' ');
  }
  Serial.print(F("\x0d\x0a"));

  Serial.print(F("UnivEnigmaWheels.UHRPairsOk: "));
  Serial.println(UnivEnigmaWheels.UHRPairsOk);

  Serial.print(F("UnivEnigmaData.UHR: "));
  Serial.println(UnivEnigmaData.UHR);

  Serial.print(F("UnivEnigmaWheels.EffectivePlugs[]: "));
  for (byte j = 0; j < 26; j++)
  {
    Serial.print((char)(UnivEnigmaWheels.EffectivePlugs[j] + 'A' - 1));
  }
  Serial.print(F("\x0d\x0a"));

  Serial.print(F("UnivEnigmaWheels.UseSWPlugs: "));
  Serial.println(UnivEnigmaWheels.UseSWPlugs);

  Serial.print(F("UnivEnigmaWheels.NeedsSaving: "));
  Serial.println(UnivEnigmaWheels.NeedsSaving);
}


void ClearEEPROM()     /*xls*/
{
  for (unsigned int i = 0; i < EEPROM.length(); i++)
  {
    EEPROM.write(i, 255);
  }
}


//BUG: write Get and Set for all members of the UnivEnigmaData structure


//
byte GetSerialFunction()     /*xls*/
{
  return UnivEnigmaData.SerialFunction;
}


//
void SetSerialFunction(byte pvalue)     /*xls*/
{
  UnivEnigmaWheels.NeedsSaving = 1;
  UnivEnigmaData.SerialFunction = pvalue;
}


//
byte GetBrightness()     /*xls*/
{
  return UnivEnigmaData.Brightness;
}


//
void SetBrightness(byte pvalue)     /*xls*/
{
  UnivEnigmaWheels.NeedsSaving = 1;
  UnivEnigmaData.Brightness = (pvalue > 250) ? 0 : (pvalue > 4) ? 4 : pvalue;
}


// Machine+UKW ID combination used by EnigmaGUI
byte GetMachineID()     /*xls*/
{
  return UnivEnigmaData.MachineID;
}


//BUG: does this need to set NeedsInit?
void SetMachineID(byte pvalue)     /*xls*/
{
  UnivEnigmaWheels.InitUKW = 1;
  UnivEnigmaWheels.NeedsSaving = 1;
  UnivEnigmaData.MachineID = pvalue;
}


//
byte GetMachineType()     /*xls*/
{
  return UnivEnigmaData.MachineType;
}


//
void SetMachineType(byte machineid)     /*xls*/
{
  UnivEnigmaWheels.NeedsInit = 1;
  UnivEnigmaWheels.RotorsChanged = 1;
  UnivEnigmaWheels.NeedsSaving = 1;
  UnivEnigmaData.MachineType = machineid;
}


//
byte GetUKWType()     /*xls*/
{
  return UnivEnigmaData.UKWType;
}


//
void SetUKWType(byte ukwid)     /*xls*/
{
  UnivEnigmaWheels.NeedsInit = 1;
  UnivEnigmaWheels.RotorsChanged = 1;
  UnivEnigmaWheels.NeedsSaving = 1;
  UnivEnigmaData.UKWType = ukwid;
}


//
byte GetRotorType(byte index)     /*xls*/
{
  return UnivEnigmaData.RotorType[index];
}


//
//BUG: check max rotor
//BUG: set corresponding ring to A
void SetRotorType(byte index, byte pvalue)     /*xls*/
{
  if (index < 4)
  {
    UnivEnigmaWheels.NeedsInit = 1;             //BUG: need to signal UpdateWheelData to copy wheels but not UKW
    UnivEnigmaWheels.RotorsChanged = 1;
    UnivEnigmaWheels.NeedsSaving = 1;

    UnivEnigmaData.RotorType[index] = pvalue;
    UnivEnigmaData.RingSetting[index] = 1;      // changed wheel, reset ring setting to A
    UnivEnigmaData.RotorPosition[index] = 'A';  // changed wheel, reset rotor position to A
  }
}


//
byte GetRingSetting(byte index)     /*xls*/
{
  return UnivEnigmaData.RingSetting[index];
}


//
void SetRingSetting(byte index, byte pvalue)     /*xls*/
{
  if (index < 4)
  {
    UnivEnigmaWheels.RotorsChanged = 1;
    UnivEnigmaWheels.NeedsSaving = 1;
    UnivEnigmaData.RingSetting[index] = ((pvalue > 0) && (pvalue < 27)) ? pvalue : 1;
  }
}


//
byte GetRotorPosition(byte index)     /*xls*/
{
  return UnivEnigmaData.RotorPosition[index];
}


//
void SetRotorPosition(byte index, byte pvalue)     /*xls*/
{
  if (index < 4)
  {
    UnivEnigmaWheels.RotorsChanged = 1;
    UnivEnigmaData.RotorPosition[index] = ((pvalue == 0) || ((pvalue > 'A' - 1) && (pvalue < 'Z' + 1))) ? pvalue : 'A';
  }
}


//
byte GetSettableUWK()     /*xls*/
{
  return UnivEnigmaWheels.SettableUWK;
}


//
byte GetNumberOfWheels()     /*xls*/
{
  return UnivEnigmaWheels.NumberOfWheels;
}


//
byte GetMaxWheels()     /*xls*/
{
  return UnivEnigmaWheels.MaxWheels;
}


//
byte GetPrintGroups()     /*xls*/
{
  return UnivEnigmaWheels.PrintGroups;
}


//
byte GetRotorsChanged()     /*xls*/
{
  return UnivEnigmaWheels.RotorsChanged;
}


//
byte GetNeedsSaving()     /*xls*/
{
  return UnivEnigmaWheels.NeedsSaving;
}


//
byte GetRotorName(byte index)     /*xls*/
{
  return UnivEnigmaWheels.RotorName[index];
}


//select stepping, lever (double stepping) or geared
byte HasLeverStep(byte pmachine)     /*xls*/
{
  byte leverstep = 1;

  switch (pmachine)
  {
    // Geared Machines
    // [A-865] Zählwerk (1928)
    case 11:
    // [G-111] (Hungary / Munich)
    case 12:
    // [G-260] (Abwehr, Argentina)
    case 13:
    // [G-312] (Abwehr / Bletchley)
    case 14:
      {
        leverstep = 0;
        break;
      }
  }

  return leverstep;
}


//
byte HasUKWD()     /*xls*/
{
  return UnivEnigmaWheels.HasUKWD;
}


//
byte HasSettableUWW(byte pmachine)     /*xls*/
{
  byte settableukw = 0;

  switch (pmachine)
  {
    // (D)(commercial)
    case 6:
    // (K) (Swiss)
    case 7:
    // (R) Rocket (Railway)
    case 8:
    // (T) Tirpitz (Japan)
    case 9:
    // [A-865] Zählwerk (1928)
    case 11:
    // [G-111] (Hungary / Munich)
    case 12:
    // [G-260] (Abwehr, Argentina)
    case 13:
    // [G-312] (Abwehr / Bletchley)
    case 14:
      {
        settableukw = 1;
        break;
      }
  }

  return settableukw;
}


//returns whether this machine has a plugboard (1) or not (0)
byte HasPlugboard(byte pmachine)     /*xls*/
{
  byte plugboard = 0;

  switch (pmachine)
  {
    // 3 wheel machines
    // I (Army; GAF)
    case 1:
    // M3 (Army; Navy)
    case 2:
    // M4
    case 3:
    // (N) Norenigma (Norway)
    case 4:
    // (S) Sonder-Enigma (Mil Amt)
    case 5:
      {
        plugboard = 1;
        break;
      }
  }

  return plugboard;
}


// returns whether this is a 3 or 4 rotor machine
byte NumberOfWheels(byte pmachine)     /*xls*/
{
  byte maxwheels = 4;

  switch (pmachine)
  {
    // 3 wheel machines
    // I (Army; GAF)
    case 1:
    // M3 (Army; Navy)
    case 2:
    // (N) Norenigma (Norway)
    case 4:
    // (S) Sonder-Enigma (Mil Amt)
    case 5:
    // (KD) (rewirable UKW-D)
    case 10:
      {
        maxwheels = 3;
        break;
      }
  }

  return maxwheels;
}


//
byte MaxNumberOfRotors(byte pmachine)     /*xls*/
{
  byte maxrotors = 3;

  switch (pmachine)
  {
    // I (Army; GAF)
    case 1:
    case 4:
      {
        maxrotors = 5;
        break;
      }
    // M3 (Army; Navy)
    case 2:
    case 3:
    case 9:
      {
        maxrotors = 8;
        break;
      }
  }

  return maxrotors;
}


//
byte MaxPrintGroups(byte pmachine)     /*xls*/
{
  byte maxgroups = 5;

  switch (pmachine)
  {
    case 3:
      {
        maxgroups = 4;
        break;
      }
  }

  return maxgroups;
}


//sets entry rotor
//returns the maximum number of machines supported by this code
byte SetETW(byte pmachine)     /*xls*/
{
  byte maxmachines = 14;

  switch (pmachine)
  {
    // I-III (D)(commercial)
    case 6:
    // I-III (K) (Swiss)
    case 7:
    // I-III (R) Rocket (Railway)
    case 8:
    // I-VIII (KD) (rewirable UKW-D)
    case 10:
    // I-III (A) [A-865] Zählwerk (1928)
    case 11:
    // I-III (G1) [G-111] (Hungary / Munich)
    case 12:
    // I-III (G2) [G-260] (Abwehr, Argentina)
    case 13:
    // I-III (G3) [G-312] (Abwehr / Bletchley)
    case 14:
      {
        //QWERTZUIOASDFGHJKPYXCVBNML -> (Q=>A) (W=>B)
        //      "abcdefghijklmnopqrstuvwxyz"
        etw = F("jwulcmnohpqzyxiradkegvbtsf");
        break;
      }

    // I-VIII (T) Tirpitz (Japan)
    case 9:
      {
        //KZROUQHYAIGBLWVSTDXFPNMCJE
        //      "abcdefghijklmnopqrstuvwxyz"
        etw = F("ilxrztkgjyamwvdufcpqeonshb");
        break;
      }

    //all other machines not mentioned above
    default:
      {
        //      "abcdefghijklmnopqrstuvwxyz"
        etw = F("abcdefghijklmnopqrstuvwxyz");
        break;
      }
  }

  //dbgPrintPROGMEM(etw);
  //dbgCheckRotor(etw);

  return maxmachines;
}


// sets reflector
// set wiring and description ("A","Bd"), same as stepping points
// returns maximum number of reflectors for this machine
byte SetUKW(byte pmachine, byte pukw)     /*xls*/
{
  byte badukw = 0;
  byte badmachine = 0;
  byte maxUKW = 1;    // by default all the machines have 1 UKW except for the cases 1,2,3

  UnivEnigmaWheels.HasUKWD = 0;

  switch (pmachine)
  {
    // I-VIII (I,M3,M4)  I (Army; GAF)  M3 (Army; Navy)  M4 "Shark" (U-Boats)
    case 1:
    case 2:
    case 3:
      {
        maxUKW = 9;
        switch (pukw)
        {
          case 1:
            {
              UnivEnigmaWheels.UKW = F("ejmzalyxvbwfcrquontspikhgd");
              UnivEnigmaWheels.UKWName = F("A");
              break;
            }
          default:
            {
              badukw = 1;
              //falls through to case 2, reflector B
            }
          case 2:
            {
              UnivEnigmaWheels.UKW = F("yruhqsldpxngokmiebfzcwvjat");
              UnivEnigmaWheels.UKWName = F("B");
              break;
            }
          case 3:
            {
              UnivEnigmaWheels.UKW = F("fvpjiaoyedrzxwgctkuqsbnmhl");
              UnivEnigmaWheels.UKWName = F("C");
              break;
            }
          case 4:
            {
              // B "thin" reflector
              UnivEnigmaWheels.UKW = F("enkqauywjicopblmdxzvfthrgs");
              UnivEnigmaWheels.UKWName = F("Bd");
              break;
            }
          case 5:
            {
              // C "thin" reflector
              UnivEnigmaWheels.UKW = F("rdobjntkvehmlfcwzaxgyipsuq");
              UnivEnigmaWheels.UKWName = F("Cd");
              break;
            }
          case 6:
            {
              //rewirable reflector, UKW needs to be a RAM array
              //BUG: rewirable reflectors have a missing pair, insert fixed portion
              //ukw = F("avboctdmezfngxhqiskrlupw");
              UnivEnigmaWheels.HasUKWD = 1;
              UnivEnigmaWheels.UKW = F("fowulaqysrtezvbxgjikdncphm");
              UnivEnigmaWheels.UKWName = F("M3");
              break;
            }
          case 7:
            {
              //rewirable reflector, UKW needs to be a RAM array
              //BUG: rewirable reflectors have a missing pair, insert fixed portion
              //ukw = F("hlknfmeiacbgdsowpzqxrutv");
              UnivEnigmaWheels.HasUKWD = 1;
              UnivEnigmaWheels.UKW = F("yolkmhjfxgdcerbvtnwqzpsiau");
              UnivEnigmaWheels.UKWName = F("L1");
              break;
            }
          case 8:
            {
              //rewirable reflector, UKW needs to be a RAM array
              //BUG: rewirable reflectors have a missing pair, insert fixed portion
              //ukw = F("agirbhcsdzewfklxmpountqv");
              UnivEnigmaWheels.HasUKWD = 1;
              UnivEnigmaWheels.UKW = F("uoxqwkmnysfpghbldvjzarecit");
              UnivEnigmaWheels.UKWName = F("L2");
              break;
            }
          case 9:
            {
              //rewirable reflector, UKW needs to be a RAM array
              //BUG: rewirable reflectors have a missing pair, insert fixed portion
              //ukw = F("hkglnqsvuxtzrwadbfcoepim");
              UnivEnigmaWheels.HasUKWD = 1;
              UnivEnigmaWheels.UKW = F("xohgjidcfenwykbsutprqzlamv");
              UnivEnigmaWheels.UKWName = F("L3");
              break;
            }
        }
        break;
      }

    // I-V (N) Norenigma (Norway)
    case 4:
      {
        UnivEnigmaWheels.UKW = F("mowjypuxndsraibfvlkzgqchet");
        UnivEnigmaWheels.UKWName = F("N");
        break;
      }

    // I-III (S) Sonder-Enigma (Mil Amt)
    case 5:
      {
        UnivEnigmaWheels.UKW = F("ciagsndrbytpzfulvhekoqxwjm");
        UnivEnigmaWheels.UKWName = F("S");
        break;
      }

    // I-III (R) Rocket (Railway)
    case 8:
      {
        UnivEnigmaWheels.UKW = F("qyhognecvpuztfdjaxwmkisrbl");
        UnivEnigmaWheels.UKWName = F("R");
        break;
      }

    // I-VIII (T) Tirpitz (Japan)
    case 9:
      {
        UnivEnigmaWheels.UKW = F("gekpbtaumocniljdxzyfhwvqsr");
        UnivEnigmaWheels.UKWName = F("T");
        break;
      }

    // I-VIII (KD) (rewirable UKW-D)
    case 10:
      {
        //rewirable reflector, UKW needs to be a RAM array
        //BUG: rewirable reflectors have a missing pair, insert fixed portion
        //ukw = F("aqbgckdielfxhzmwnvotpurs");
        UnivEnigmaWheels.HasUKWD = 1;
        UnivEnigmaWheels.UKW = F("kotvpnlmjiaghfbewyxczdqsru");
        UnivEnigmaWheels.UKWName = F("KD");
        break;
      }

    // I-III (G3) [G-312] (Abwehr / Bletchley)
    case 14:
      {
        UnivEnigmaWheels.UKW = F("rulqmzjsygocetkwdahnbxpvif");
        UnivEnigmaWheels.UKWName = F("G");
        break;
      }

    // Arb.W (maintenance wheel)
    case 15:
      {
        UnivEnigmaWheels.UKW = F("onvsrgfjuhpmlbakwedzicqyxt");
        UnivEnigmaWheels.UKWName = F("M");
        break;
      }

    // I-III (D)(commercial)
    case 6:
    // I-III (K) (Swiss)
    case 7:
    // I-III (A) [A-865] Zählwerk (1928)
    case 11:
    // I-III (G1) [G-111] (Hungary / Munich)
    case 12:
    // I-III (G2) [G-260] (Abwehr, Argentina)
    case 13:
    default:
      {
        UnivEnigmaWheels.UKW = F("imetcgfraysqbzxwlhkdvupojn");
        UnivEnigmaWheels.UKWName = F("CM");
        break;
      }
  }

  //dbgPrintPROGMEM(ukw); //BUG: remove
  //dbgCheckRotor(ukw);

  return maxUKW;
}


// returns the maximum number of wheels for that machine (3 or 4)
byte SetCurrentWheel(byte pmachine, byte pwheel)     /*xls*/
{
  byte badwheel = 0;
  byte badmachine = 0;
  byte maxWheel = 3;      // by default, each machine has a max of 3 wheels

  currentwheelname = '0' + pwheel;

  switch (pmachine)
  {
    // I-VIII (I,M3,M4)  I (Army; GAF)  M3 (Army; Navy)  M4 "Shark" (U-Boats)
    case 1:
    case 2:
    case 3:
      {
        if (pmachine == 1)
        {
          maxWheel = 5;
        }
        else
        {
          maxWheel = 8;  // for right,middle and left, 4th rotor is only beta or gamma
        }

        switch (pwheel)
        {
          case 1:
            {
              currentwheel = F("ekmflgdqvzntowyhxuspaibrcj");
              currentstepnotches = F("q");
              break;
            }
          case 2:
            {
              currentwheel = F("ajdksiruxblhwtmcqgznpyfvoe");
              currentstepnotches = F("e");
              break;
            }
          case 3:
            {
              currentwheel = F("bdfhjlcprtxvznyeiwgakmusqo");
              currentstepnotches = F("v");
              break;
            }
          case 4:
            {
              currentwheel = F("esovpzjayquirhxlnftgkdcmwb");
              currentstepnotches = F("j");
              break;
            }
          case 5:
            {
              currentwheel = F("vzbrgityupsdnhlxawmjqofeck");
              currentstepnotches = F("z");
              break;
            }
          case 6:
            {
              currentwheel = F("jpgvoumfyqbenhzrdkasxlictw");
              currentstepnotches = F("zm");
              break;
            }
          case 7:
            {
              currentwheel = F("nzjhgrcxmyswboufaivlpekqdt");
              currentstepnotches = F("zm");
              break;
            }
          case 8:
            {
              currentwheel = F("fkqhtlxocbjspdzramewniuygv");
              currentstepnotches = F("zm");
              break;
            }
          // beta
          case 9:
            {
              currentwheel = F("leyjvcnixwpbqmdrtakzgfuhos");
              currentstepnotches = F("");
              currentwheelname = 'B';
              break;
            }
          // gamma
          case 10:
            {
              currentwheel = F("fsokanuerhmbtiycwlqpzxvgjd");
              currentstepnotches = F("");
              currentwheelname = 'G';
              break;
            }
          default:
            {
              badwheel = 1;
              break;
            }
        }
        break;
      }

    // I-V (N) Norenigma (Norway)
    case 4:
      {
        maxWheel = 5;

        switch (pwheel)
        {
          case 1:
            {
              currentwheel = F("wtokasuyvrbxjhqcpzefmdinlg");
              currentstepnotches = F("q");
              break;
            }
          case 2:
            {
              currentwheel = F("gjlpubswemctqvhxaofzdrkyni");
              currentstepnotches = F("e");
              break;
            }
          case 3:
            {
              currentwheel = F("jwfmhnbpusdytixvzgrqlaoekc");
              currentstepnotches = F("v");
              break;
            }
          case 4:
            {
              currentwheel = F("esovpzjayquirhxlnftgkdcmwb");
              currentstepnotches = F("j");
              break;
            }
          case 5:
            {
              currentwheel = F("hejxqotzbvfdascilwpgynmurk");
              currentstepnotches = F("z");
              break;
            }
          default:
            {
              badwheel = 1;
              break;
            }
        }
        break;
      }

    // I-III (S) Sonder-Enigma (Mil Amt)
    case 5:
      {
        switch (pwheel)
        {
          case 1:
            {
              currentwheel = F("veosirzujdqckgwypnxaflthmb");
              currentstepnotches = F("q");
              break;
            }
          case 2:
            {
              currentwheel = F("uemoatqlshpkcyfwjzbgvxindr");
              currentstepnotches = F("e");
              break;
            }
          case 3:
            {
              currentwheel = F("tzhxmbsipnurjfdkeqvcwglaoy");
              currentstepnotches = F("v");
              break;
            }
          default:
            {
              badwheel = 1;
              break;
            }
        }
        break;
      }

    // I-III (D)(commercial)
    case 6:
      {
        maxWheel = 4;

        switch (pwheel)
        {
          case 1:
            {
              currentwheel = F("lpgszmhaeoqkvxrfybutnicjdw");
              currentstepnotches = F("y");
              break;
            }
          case 2:
            {
              currentwheel = F("slvgbtfxjqohewirzyamkpcndu");
              currentstepnotches = F("e");
              break;
            }
          case 3:
            {
              currentwheel = F("cjgdpshkturawzxfmynqobvlie");
              currentstepnotches = F("n");
              break;
            }
          default:
            {
              badwheel = 1;
              break;
            }
        }
        break;
      }

    // I-III (K) (Swiss)
    case 7:
      {
        maxWheel = 4;

        switch (pwheel)
        {
          case 1:
            {
              currentwheel = F("pezuohxscvfmtbglrinqjwaydk");
              currentstepnotches = F("y");
              break;
            }
          case 2:
            {
              currentwheel = F("zouesydkfwpciqxhmvblgnjrat");
              currentstepnotches = F("e");
              break;
            }
          case 3:
            {
              currentwheel = F("ehrvxgaobqusimzflynwktpdjc");
              currentstepnotches = F("n");
              break;
            }
          default:
            {
              badwheel = 1;
              break;
            }
        }
        break;
      }

    // I-III (R) Rocket (Railway)
    case 8:
      {
        maxWheel = 4;

        switch (pwheel)
        {
          case 1:
            {
              currentwheel = F("jgdqoxuscamifrvtpnewkblzyh");
              currentstepnotches = F("n");
              break;
            }
          case 2:
            {
              currentwheel = F("ntzpsfbokmwrcjdivlaeyuxhgq");
              currentstepnotches = F("e");
              break;
            }
          case 3:
            {
              currentwheel = F("jviubhtcdyakeqzposgxnrmwfl");
              currentstepnotches = F("y");
              break;
            }
          default:
            {
              badwheel = 1;
              break;
            }
        }
        break;
      }

    // I-VIII (T) Tirpitz (Japan)
    case 9:
      {
        maxWheel = 4;

        switch (pwheel)
        {
          case 1:
            {
              currentwheel = F("kptyuelocvgrfqdanjmbswhzxi");
              currentstepnotches = F("wzekq");
              break;
            }
          case 2:
            {
              currentwheel = F("uphzlweqmtdjxcaksoigvbyfnr");
              currentstepnotches = F("wzflr");
              break;
            }
          case 3:
            {
              currentwheel = F("qudlyrfekonvzaxwhmgpjbsict");
              currentstepnotches = F("wzekq");
              break;
            }
          case 4:
            {
              currentwheel = F("ciwtbkxnrespflydagvhquojzm");
              currentstepnotches = F("wzflr");
              break;
            }
          case 5:
            {
              currentwheel = F("uaxgisnjbverdylfzwtpckohmq");
              currentstepnotches = F("ycfkr");
              break;
            }
          case 6:
            {
              currentwheel = F("xfuzgalvhcnysewqtdmrbkpioj");
              currentstepnotches = F("xeimq");
              break;
            }
          case 7:
            {
              currentwheel = F("bjvftxplnayozikwgdqeruchsm");
              currentstepnotches = F("ycfkr");
              break;
            }
          case 8:
            {
              currentwheel = F("ymtpnzhwkodajxeluqvgcbisfr");
              currentstepnotches = F("xeimq");
              break;
            }
          default:
            {
              badwheel = 1;
              break;
            }
        }
        break;
      }

    // I-VIII (KD) (rewirable UKW-D)
    case 10:
      {
        switch (pwheel)
        {
          case 1:
            {
              currentwheel = F("veziojcxkyduntwaplqgbhsfmr");
              currentstepnotches = F("suyaehlnq");
              break;
            }
          case 2:
            {
              currentwheel = F("hgrbsjzetdlvpmqycxaokinfuw");
              currentstepnotches = F("suyaehlnq");
              break;
            }
          case 3:
            {
              currentwheel = F("nwlhxgrbyojsazdvtpkfqmeuic");
              currentstepnotches = F("suyaehlnq");
              break;
            }
          default:
            {
              badwheel = 1;
              break;
            }
        }
        break;
      }

    // I-III (A) [A-865] Zählwerk (1928)
    case 11:
      {
        maxWheel = 4;

        switch (pwheel)
        {
          case 1:
            {
              currentwheel = F("lpgszmhaeoqkvxrfybutnicjdw");
              currentstepnotches = F("suvwzabcefgiklopq");
              break;
            }
          case 2:
            {
              currentwheel = F("slvgbtfxjqohewirzyamkpcndu");
              currentstepnotches = F("stvyzacdfghkmnq");
              break;
            }
          case 3:
            {
              currentwheel = F("cjgdpshkturawzxfmynqobvlie");
              currentstepnotches = F("uwxaefhkmnr");
              break;
            }
          default:
            {
              badwheel = 1;
              break;
            }
        }
        break;
      }

    // I-III (G1) [G-111] (Hungary / Munich)
    case 12:
      {
        maxWheel = 4;

        switch (pwheel)
        {
          case 1:
            {
              currentwheel = F("wlrhbqundkjczsexotmagyfpvi");
              currentstepnotches = F("suvwzabcefgiklopq");
              break;
            }
          case 2:
            {
              currentwheel = F("tfjqazwmhlcuixrdygoevbnskp");
              currentstepnotches = F("stvyzacdfghkmnq");
              break;
            }
          case 3:
            {
              currentwheel = F("qtpixwvdfrmusljohcanezkybg");
              currentstepnotches = F("swzfhmq");
              currentwheelname = '5';
              break;
            }
          default:
            {
              badwheel = 1;
              break;
            }
        }
        break;
      }

    // I-III (G2) [G-260] (Abwehr, Argentina)
    case 13:
      {
        maxWheel = 4;

        switch (pwheel)
        {
          case 1:
            {
              currentwheel = F("rcspblkqaumhwytifzvgojnexd");
              currentstepnotches = F("suvwzabcefgiklopq");
              break;
            }
          case 2:
            {
              currentwheel = F("wcmibvpjxarosgndlzkeyhufqt");
              currentstepnotches = F("stvyzacdfghkmnq");
              break;
            }
          case 3:
            {
              currentwheel = F("fvdhzelsqmaxokyiwpgcbujtnr");
              currentstepnotches = F("uwxaefhkmnr");
              break;
            }
          default:
            {
              badwheel = 1;
              break;
            }
        }
        break;
      }

    // I-III (G3) [G-312] (Abwehr / Bletchley)
    case 14:
      {
        maxWheel = 4;

        switch (pwheel)
        {
          case 1:
            {
              currentwheel = F("dmtwsilruyqnkfejcazbpgxohv");
              currentstepnotches = F("suvwzabcefgiklopq");
              break;
            }
          case 2:
            {
              currentwheel = F("hqzgpjtmoblncifdyawveusrkx");
              currentstepnotches = F("stvyzacdfghkmnq");
              break;
            }
          case 3:
            {
              currentwheel = F("uqntlszfmrehdpxkibvygjcwoa");
              currentstepnotches = F("uwxaefhkmnr");
              break;
            }
          default:
            {
              badwheel = 1;
              break;
            }
        }
        break;
      }

    // Arb.W (maintenance wheel)
    case 15:
      {
        switch (pwheel)
        {
          case 1:
            {
              currentwheel = F("abcdefghijklmnopqrstuvwxyz");
              currentstepnotches = F("");
              break;
            }
          // made up wheel, straight through coding, steps at all notches
          case 2:
            {
              currentwheel = F("abcdefghijklmnopqrstuvwxyz");
              currentstepnotches = F("abcdefghijklmnopqrstuvwxyz");
              break;
            }
          default:
            {
              badwheel = 1;
              break;
            }
        }
        break;
      }

    //unknown machine
    default:
      {
        badmachine = 1;
        badwheel = 1;
        break;
      }
  }

  if (badwheel)
  {
    currentwheel = F("abcdefghijklmnopqrstuvwxyz");
    currentstepnotches = F("");
  }

  //dbgPrintPROGMEM(currentwheel);
  //dbgCheckRotor(currentwheel);

  return maxWheel;
}


//
void LoadEnigmaFromEEPROM()     /*xls*/
{
  univEnigmaData_t UnivEnigmaData1;

  //BUG: turn displays off

  // read eeprom data
  eeprom_read_block((void*)&UnivEnigmaData, (void*)EEPROMADD, ENIGMADATASIZE);               // load first block onto working structure
  eeprom_read_block((void*)&UnivEnigmaData1, (void*)EEPROMADD1, ENIGMADATASIZE);             // and second block onto temporary structure

  if ((unsigned char)(UnivEnigmaData.Ver2 - UnivEnigmaData.Ver1) != (unsigned char)1)        // if first block is corrupted
  {
    eeprom_read_block((void*)&UnivEnigmaData, (void*)EEPROMADD1, ENIGMADATASIZE);            // load second block onto working structure
    eeprom_write_block((const void*)&UnivEnigmaData, (void*)EEPROMADD, ENIGMADATASIZE);      // and save working structure into first block
  }

  if ((unsigned char)(UnivEnigmaData1.Ver2 - UnivEnigmaData1.Ver1) != (unsigned char)1)      // if second block is corrupted
  {
    eeprom_write_block((const void*)&UnivEnigmaData, (void*)EEPROMADD1, ENIGMADATASIZE);     // save working structure into second block
  }
}


//
byte SaveEnigmaToEEPROM()     /*xls*/
{
  //BUG: turn displays off

  byte v = UnivEnigmaWheels.NeedsSaving;

  if (v)
  {
    UnivEnigmaWheels.NeedsSaving = 0;

    UnivEnigmaData.Ver1 = UnivEnigmaData.Ver1 + 1; // 0->1
    UnivEnigmaData.Ver2 = UnivEnigmaData.Ver1 + 1; // 1->2

    //Serial.print('s'); //BUG: remove
    //dbgPrintEnigma(); //BUG: remove

    eeprom_write_block((const void*)&UnivEnigmaData, (void*)EEPROMADD, ENIGMADATASIZE);
    eeprom_write_block((const void*)&UnivEnigmaData, (void*)EEPROMADD1, ENIGMADATASIZE);

    //Serial.print('d'); //BUG: remove
  }

  return v;
}


//
void CopyUKW(const __FlashStringHelper * srcUKW)     /*xls*/
{
  //const PROGMEM char *p = (const char PROGMEM *)srcUKW;
  const char *p = (const char *)srcUKW;

  char k = 0;

  for (byte i = 0; i < 26; i++)
  {
    k = pgm_read_byte(p + i);
    UnivEnigmaData.UKWD[i] = k;
  }
}


//
void CopyStepPoints(const __FlashStringHelper * srcSteps, byte tWheel)     /*xls*/
{
  //const PROGMEM char *p = (const char PROGMEM *)srcSteps;
  const char *p = (const char *)srcSteps;

  char k = 1; //initialize to non-zero value, the first 0 stops reading sourcesteps, but keeps writing 0

  byte index = tWheel * 26;

  //copies a maximum of 26 bytes of data, if the string is shorter it fills with 0 to indicate the end
  for (byte i = 0; i < 26; i++)
  {
    //BUG: modify this to fill the rest of the buffer with 0, when going from a machine with more step points to one with less
    if (k != 0)
    {
      k = pgm_read_byte(p + i);
    }

    UnivEnigmaData.StepPoints[index + i] = k;
  }
}


//
void InitSWPlugs()    /*xls*/
{
  for (byte i = 0; i < 26; i++)
  {
    UnivEnigmaData.PlugPairs[i] = 0;  //BUG: need to re-initialize this if machine is changed, move to initPlugs()
  }

  UnivEnigmaWheels.UseSWPlugs = 0;
  UnivEnigmaWheels.UHRPairsOk = 0;

}


// index is 0 based (0..12), LR is 0 for left pair, 1 for right pair, val is 1 based (A..Z, A==1)
byte GetPlugPair(byte index, byte LR)    /*xls*/
{
  return UnivEnigmaData.PlugPairs[(index * 2) + LR];
}


// index is 0 based (0..12), LR is 0 for left pair, 1 for right pair, val is 1 based (A..Z, A==1)
void SetPlugPair(byte index, byte LR, byte val)    /*xls*/
{
  if ((index < 13) && (LR < 2) && (val < 27))
  {
    UnivEnigmaWheels.NeedsSaving = 1;
    UnivEnigmaWheels.UHRPairsOk = 0;
    UnivEnigmaData.UHR = 0;
    UnivEnigmaData.PlugPairs[(index * 2) + LR] = val;
  }
}


//
byte GetUHR()    /*xls*/
{
  return UnivEnigmaData.UHR;
}


//
void SetUHR(byte val)    /*xls*/
{
  if (val < 41)
  {
    UnivEnigmaWheels.NeedsSaving = 1;
    UnivEnigmaData.UHR = val;
  }
}


//
void EnableSWPlugs(byte val)    /*xls*/
{
  UnivEnigmaWheels.UseSWPlugs = val;
}


byte UHRPairsOK()    /*xls*/
{
  return UnivEnigmaWheels.UHRPairsOk;
}


//BUG: when to call this upon boot to set up UHR? call this from menu &  call ActivateUHR();
//
byte CheckPlugPairs()    /*xls*/
{
  byte Pairs[26];
  byte leftp, rightp;
  byte mismatchpair = 0;
  byte paircount = 0;

  UnivEnigmaWheels.UHRPairsOk = 1;

  for (byte i = 0; i < 26; i++)
  {
    Pairs[i] = 0;
    UnivEnigmaWheels.EffectivePlugs[i] = i + 1;
  }

  for (byte i = 0; i < 13; i++)
  {
    leftp = GetPlugPair(i, 0);
    rightp = GetPlugPair(i, 1);

    if (i < 10)
    {
      if ((leftp == 0) || (rightp == 0))
      {
        UnivEnigmaWheels.UHRPairsOk = 0;
      }
    }
    else
    {
      if ((leftp != 0) || (rightp != 0))
      {
        UnivEnigmaWheels.UHRPairsOk = 0;
      }
    }

    //Serial.print(leftp); //BUG: remove
    //Serial.print(' ');
    //Serial.println(rightp);

    if (leftp)
    {
      if (rightp == 0)
      {
        mismatchpair = 1;
      }
      UnivEnigmaWheels.EffectivePlugs[leftp - 1] = rightp;
      Pairs[leftp - 1]++;
      paircount++;
    }

    if (rightp)
    {
      if (leftp == 0)
      {
        mismatchpair = 1;
      }
      UnivEnigmaWheels.EffectivePlugs[rightp - 1] = leftp;
      Pairs[rightp - 1]++;
      paircount++;
    }
  }

  for (byte i = 0; i < 26; i++)
  {
    //Serial.print(Pairs[i]); // BUG: remove
    if (Pairs[i] > 1)
    {
      mismatchpair = 1;
    }
  }

  if (mismatchpair)
  {
    paircount = 99;
    UnivEnigmaWheels.UseSWPlugs = 0;
  }
  else
  {
    paircount /= 2;
    UnivEnigmaWheels.UseSWPlugs = paircount;

    if (UnivEnigmaWheels.UHRPairsOk)
    {
      ActivateUHR();
    }
  }

  return paircount;
}


//
void ActivateUHR()    /*xls*/
{
  const __FlashStringHelper *UHRSF = F("\x06\x1F\x04\x1D\x12\x27\x10\x19\x1E\x17\x1C\x01\x26\x0B\x24\x25\x1A\x1B\x18\x15\x0E\x03\x0C\x11\x02\x07\x00\x21\x0A\x23\x08\x05\x16\x13\x14\x0D\x22\x0F\x20\x09");
  const __FlashStringHelper *UHRPLUGSF =  F("\x06\x00\x07\x05\x01\x08\x04\x02\x09\x03");

  //const PROGMEM char *uhrptr = (const char PROGMEM *)UHRSF;
  //const PROGMEM char *uhrplugptr = (const char PROGMEM *)UHRPLUGSF;

  const char *uhrptr = (const char *)UHRSF;
  const char *uhrplugptr = (const char *)UHRPLUGSF;

  //byte ndx = 0;

  if (UnivEnigmaWheels.UHRPairsOk)
  {
    for (byte i = 0; i < 26; i++)
    {
      UnivEnigmaWheels.EffectivePlugs[i] = i + 1;
    }

    for (byte i = 0; i < 10; i++)
    {
      byte pin = 0;
      byte pinright = 0;
      byte pinleft = 0;

      pin = UnivEnigmaData.UHR + i * 4;
      if (pin > 39)
      {
        pin -= 40;
      }

      for (byte j = 0; j < 40; j++)
      {
        if (pgm_read_byte(uhrptr + j) == pin)
        {
          pinleft = j;
        }
      }

      pinright = pgm_read_byte(uhrptr + pin);

      //these two need to be signed, see <0 below
      //char is signed -127..+128
      char plugright;
      char plugleft;

      plugright = (pinright - (UnivEnigmaData.UHR + 2));
      if (plugright < 0)
      {
        plugright += 40;
      }
      plugright = plugright / 4;

      plugleft = (pinleft - (UnivEnigmaData.UHR + 2));
      if (plugleft < 0)
      {
        plugleft += 40;
      }
      plugleft = plugleft / 4;

      //EffSTECKER[EnigmaData.PAIRS[i * 2] - 65] = EnigmaData.PAIRS[pgm_read_byte(uhrplugptr + plugright) * 2 + 1];
      //EffSTECKER[EnigmaData.PAIRS[pgm_read_byte(uhrplugptr + i) * 2 + 1] - 65] = EnigmaData.PAIRS[plugleft * 2];

      //arrays are zero based (A==0), values inside are 1 based (A==1)
      UnivEnigmaWheels.EffectivePlugs[GetPlugPair(i, 0) - 1] = GetPlugPair(pgm_read_byte(uhrplugptr + plugright), 1);
      UnivEnigmaWheels.EffectivePlugs[GetPlugPair(pgm_read_byte(uhrplugptr + i), 1) - 1] = GetPlugPair(plugleft, 0);
    }
  }

  //BUG: remove
  /*
    Serial.print(F("Stecker/Uhr:"));
    for (byte i = 0; i < 26; i++)
    {
    //Serial.print(UnivEnigmaWheels.EffectivePlugs[i]);
    //Serial.print(' ');
    Serial.print((char)(UnivEnigmaWheels.EffectivePlugs[i] + 64));
    }
    Serial.print(F("\x0d\x0a"));
  */
}


//
//BUG: call this function manually after UpdateWheelData when needed (right after setting a machine) (if eeprom is corrupted)
void ActivateUKWD()    /*xls*/
{
  //Serial.println(F("activating UKWD")); //BUG: remove
  CopyUKW(UnivEnigmaWheels.UKW);
}


//
//
void UpdateWheelData()   /*xls*/
{
  UnivEnigmaWheels.NeedsInit = 0;

  UnivEnigmaWheels.LeverStepping = HasLeverStep(UnivEnigmaData.MachineType);
  UnivEnigmaWheels.SettableUWK = HasSettableUWW(UnivEnigmaData.MachineType);
  UnivEnigmaWheels.Plugboard = HasPlugboard(UnivEnigmaData.MachineType);
  UnivEnigmaWheels.NumberOfWheels = NumberOfWheels(UnivEnigmaData.MachineType);
  UnivEnigmaWheels.MaxWheels = MaxNumberOfRotors(UnivEnigmaData.MachineType);
  UnivEnigmaWheels.PrintGroups = MaxPrintGroups(UnivEnigmaData.MachineType);

  SetETW(UnivEnigmaData.MachineType);
  UnivEnigmaWheels.ETW = etw;

  // ATTN: sets HasUKWD, but does not copy UKW to UnivEnigmaWheels.UKW[]
  SetUKW(UnivEnigmaData.MachineType, UnivEnigmaData.UKWType); // caller must decide whether to call ActivateUKWD() manually

  for (byte i = 0; i < 4; i++)
  {
    SetCurrentWheel(UnivEnigmaData.MachineType, UnivEnigmaData.RotorType[i]);
    UnivEnigmaWheels.Wheels[i] = currentwheel;
    UnivEnigmaWheels.RotorName[i] = currentwheelname;
    CopyStepPoints(currentstepnotches, i);
  }

  UnivEnigmaWheels.InitUKW = 0;
}


//
void DefaultEnigma()     /*xls*/
{
  UnivEnigmaData.Init1 = 3;
  UnivEnigmaData.Init2 = 7;
  UnivEnigmaData.Ver1 = 0;
  UnivEnigmaData.Ver2 = 1;

  UnivEnigmaData.SerialFunction = 0;    // no output to serial port
  UnivEnigmaData.Brightness = 2;

  // defaults match Palloks universal enigma
  // use ID value on EningmaGUI->DisplayModel()
  // Machine ID of 9 is M4 with Beta greek wheel
  UnivEnigmaData.MachineID = DEFAULTMACHINEID;
  // loads MachineType,UKWType,RotorType,RotorPosition matching MachineID
  // DisplayModel loads model name on display, make sure ShowRotorPositions(); follows this call, see setup()
  DisplayModel(UnivEnigmaData.MachineID, 1);

  //BUG: create set/get and move to EnigmaGUI
  for (byte i = 0; i < 104; i++)
  {
    UnivEnigmaData.StepPoints[i] = 0;
  }

  InitSWPlugs();

  UnivEnigmaWheels.NeedsInit = 1;
  UnivEnigmaWheels.RotorsChanged = 0;
  UnivEnigmaWheels.NeedsSaving = 1;
}


// determine if struct contains valid data
void InitEnigma()     /*xls*/
{
  byte skiploadingukw = 1;

  //load defaults if data seems corrupted
  if ((UnivEnigmaData.Init1 != 3) || (UnivEnigmaData.Init2 != 7))
  {
    DefaultEnigma();
    skiploadingukw = 0;
  }

  //use enigmadata to initialize the rest of the rotors
  UpdateWheelData();

  if (!skiploadingukw)
  {
    ActivateUKWD();
    InitSWPlugs();
  }
}


//BUG: remove to enable printstep (look for another definition in MegaEnigma.ino)
//define MegaEnigma


//
void printstep(const __FlashStringHelper * stepdesc, byte pletter)    /*xls*/
{
#if not defined MegaEnigma
  Serial.print(stepdesc);
  Serial.print(F(">"));
  Serial.print((char)('A' + pletter - 1));
  Serial.print(F(">"));
#endif
}


//
void dbgkey(const __FlashStringHelper * desc, byte k)    /*xls*/
{
  return; // BUG: disable

  Serial.print(desc);
  Serial.print((byte)k);
  Serial.print(F(" "));
  Serial.println((char)k);
}


//lookup [(rotor pos+ring+key)%26]
// rotor is 0,1,2,3
byte lookuprotorfwd(const __FlashStringHelper * srcRotor, byte rotorpos, byte ring, byte key)    /*xls*/
{
  //const PROGMEM char *r = (const char PROGMEM *)srcRotor;
  const char *r = (const char *)srcRotor;
  byte p;
  byte k = key + 'A' - 1;

  dbgkey(F("rotor "), 0);
  dbgkey(F("rotorpos "), rotorpos);
  dbgkey(F("ring "), ring);
  dbgkey(F("key "), k);

  p = rotorpos - (ring - 1);

  dbgkey(F("p "), p);

  if (p < 'A')
  {
    p += 26;
  }

  dbgkey(F("p "), p);

  k = k + (p - 'A');

  dbgkey(F("k "), k);

  if (k > 'Z')
  {
    k = k - ('Z' + 1);
  }
  else
  {
    k = k - 'A';
  }

  dbgkey(F("k "), k);

  k = pgm_read_byte(r + k) - 32;

  dbgkey(F("k "), k);

  p = rotorpos - (ring - 1);

  dbgkey(F("p "), p);

  if (p < 'A')
  {
    p += 26;
  }

  dbgkey(F("p "), p);

  k = k - (p - 'A');

  dbgkey(F("k "), k);

  if (k < 'A')
  {
    k = k + 26;
  }

  dbgkey(F("k "), k);

  k = k - 'A' + 1;
  return k;
}


//
byte lookupukw(byte key)     /*xls*/
{
  byte k = UnivEnigmaData.UKWD[key - 1] - 'a' + 1;

  return k;
}


//
byte checkUKWD()     /*xls*/
{
  byte i = 0;

  byte contacts[26];

  byte error = 0;

  for (i = 0; i < 26; i++)
  {
    contacts[i] = 0;
  }

  for (i = 0; i < 26; i++)
  {
    contacts[UnivEnigmaData.UKWD[i] - 'a']++;
  }

  for (i = 0; i < 26; i++)
  {
    if (contacts[i] != 1)
    {
      error++;
    }
  }

  // 0 means all good!
  return error;
}


//
void changeukw(byte index, byte pvalue)     /*xls*/
{
  UnivEnigmaWheels.NeedsSaving = 1;
  if ((index < 26) && (pvalue > 'a' - 1) && (pvalue < 'z' + 1))
  {
    UnivEnigmaData.UKWD[index] = pvalue;
  }
}


//
byte BPtoUD(byte key)     /*xls*/
{
  const __FlashStringHelper *BPTOUD = F("A-ZXWVUTSRQPON-MLKIHGFEDCB");
  //const PROGMEM char *bp = (const char PROGMEM *)BPTOUD;
  const char *bp = (const char *)BPTOUD;

  return pgm_read_byte(bp + key - 1);
}


//
byte UDtoBP(byte key)     /*xls*/
{
  const __FlashStringHelper *UDTOBP = F("AZYXWVUTS-RQPNMLKJIHGFED-C");
  //const PROGMEM char *ud = (const char PROGMEM *)UDTOBP;
  const char *ud = (const char *)UDTOBP;

  return pgm_read_byte(ud + key - 1);
}


//
byte translateBPtoUD(byte key)     /*xls*/
{
  byte v;

  //J=Y
  if (key == 10)
  {
    return 25;
  }

  if (key == 25)
  {
    return 10;
  }

  //v = pgm_read_byte(ud + key - 1) - 'A' + 1; //key is 1==A... in BP notation
  v = UDtoBP(key) - 'A' + 1; //key is 1==A... in BP notation

  v = lookupukw(v);

  //v = pgm_read_byte(bp + v - 1) - 'A' + 1;
  v = BPtoUD(v) - 'A' + 1;

  return v;
}


//
void dbgkeybwd(const __FlashStringHelper * desc, byte k)     /*xls*/
{
  return;  //BUG: disable

  Serial.print(desc);
  Serial.print((byte)k);
  Serial.print(F(" "));
  Serial.println((char)k);
}


//
byte lookuprotorbwd(const __FlashStringHelper * srcRotor, byte rotorpos, byte ring, byte key)    /*xls*/
{
  //const PROGMEM char *r = (const char PROGMEM *)srcRotor;
  const char *r = (const char *)srcRotor;
  byte p;
  byte k = key + 'A' - 1;
  byte k1;

  dbgkeybwd(F("bwrotor "), 0);
  dbgkeybwd(F("rotorpos "), rotorpos);
  dbgkeybwd(F("ring "), ring);
  dbgkeybwd(F("key "), k);

  p = rotorpos - (ring - 1);

  dbgkeybwd(F("p "), k);

  if (p < 'A')
  {
    p += 26;
  }

  dbgkeybwd(F("p "), k);

  k = k + (p - 'A');

  dbgkeybwd(F("k "), k);

  if (k > 'Z')
  {
    k = k - 26;
  }

  dbgkeybwd(F("k "), k);

  for (byte j = 0; j < 26; j++)
  {
    if ((pgm_read_byte(r + j) - 32) == k)
    {
      k1 = 'A' + j;
    }
  }

  k = k1;

  dbgkeybwd(F("k "), k);

  p = rotorpos - (ring - 1);

  dbgkeybwd(F("p "), k);

  if (p < 'A')
  {
    p += 26;
  }

  dbgkeybwd(F("p "), k);

  k = k - (p - 'A');

  dbgkeybwd(F("k "), k);

  if (k < 'A')
  {
    k = k + 26;
  }

  dbgkeybwd(F("k "), k);

  k = k - 'A' + 1;
  return k;
}


// rotor is <left> 3,2,1,0 <right>
byte isStep(byte rotor)     /*xls*/
{
  byte now = 0;

  for (byte j = 0; j < 26; j++)
  {
    if (UnivEnigmaData.StepPoints[rotor * 26 + j] - 32 == UnivEnigmaData.RotorPosition[rotor])
    {
      now = 1;
    }
  }

  return now;
}


// rotor is <left> 3,2,1,0 <right>
byte isAfterStep(byte rotor)     /*xls*/
{
  byte now = 0;
  byte current = UnivEnigmaData.RotorPosition[rotor] - 1;

  if (current < 'A')
  {
    current = 'Z';
  }

  for (byte j = 0; j < 26; j++)
  {
    if (UnivEnigmaData.StepPoints[rotor * 26 + j] - 32 == current)
    {
      now = 1;
    }
  }

  return now;
}


//
byte IsWheelLockedInc(byte index)     /*xls*/
{
#if not defined UnlockWheels
  return UnivEnigmaWheels.wheellockedinc[index];
#else
  return 0;
#endif
}


//
byte IsWheelLockedDec(byte index)     /*xls*/
{
#if not defined UnlockWheels
  return UnivEnigmaWheels.wheellockeddec[index];
#else
  return 0;
#endif
}


//BUG: check this logic for middle wheels 1 & 2
//
void EnigmaCheckLeverLocked()     /*xls*/
{
  UnivEnigmaWheels.wheellockeddec[0] = 1;
  UnivEnigmaWheels.wheellockeddec[1] = isAfterStep(0);
  UnivEnigmaWheels.wheellockeddec[2] = isAfterStep(1);
  UnivEnigmaWheels.wheellockeddec[3] = 0;

  UnivEnigmaWheels.wheellockeddec[1] = UnivEnigmaWheels.wheellockeddec[1] + UnivEnigmaWheels.wheellockeddec[2]; // middle rotor double step logic

  UnivEnigmaWheels.wheellockedinc[0] = 0;
  UnivEnigmaWheels.wheellockedinc[1] = 0;
  UnivEnigmaWheels.wheellockedinc[2] = 0;
  UnivEnigmaWheels.wheellockedinc[3] = 0;
}


//
void EnigmaLeverStep()     /*xls*/
{
  byte advance[3];

  advance[0] = 1;
  advance[1] = isStep(0);
  advance[2] = isStep(1);

  advance[1] = advance[1] + advance[2]; // middle rotor double step logic

  for (byte i = 0; i < 3; i++)
  {
    if (advance[i])
    {
      UnivEnigmaData.RotorPosition[i]++;
      if (UnivEnigmaData.RotorPosition[i] > 'Z')
      {
        UnivEnigmaData.RotorPosition[i] = 'A';
      }
    }
  }

  EnigmaCheckLeverLocked();
}


// signals to EnigmaGUI that all wheels are locked and cannot be changed when a key is pressed.
void EnigmaCheckGearLocked()     /*xls*/
{
  UnivEnigmaWheels.wheellockeddec[0] = 1;
  UnivEnigmaWheels.wheellockeddec[1] = 1;
  UnivEnigmaWheels.wheellockeddec[2] = 1;
  UnivEnigmaWheels.wheellockeddec[3] = 1;

  UnivEnigmaWheels.wheellockedinc[0] = 1;
  UnivEnigmaWheels.wheellockedinc[1] = 1;
  UnivEnigmaWheels.wheellockedinc[2] = 1;
  UnivEnigmaWheels.wheellockedinc[3] = 1;
}


//BUG: Test this
//
void EnigmaGearStep()     /*xls*/
{
  byte incnow = 1;
  byte incnext = 1;

  for (byte i = 0; i < 4; i++)
  {
    if ((incnext == 1) && (isStep(i) == 0))
    {
      incnext = 0;
    }
    if (incnow)
    {
      UnivEnigmaData.RotorPosition[i]++;
      if (UnivEnigmaData.RotorPosition[i] > 'Z')
      {
        UnivEnigmaData.RotorPosition[i] = 'A';
      }
    }
    incnow = incnext;
  }

  EnigmaCheckGearLocked();
}


//
void EnigmaCheckWheelLocked()     /*xls*/
{
  if (UnivEnigmaWheels.LeverStepping)
  {
    EnigmaCheckLeverLocked();
  }
  else
  {
    EnigmaCheckGearLocked();
  }
}


//
void EnigmaStep()     /*xls*/
{
  UnivEnigmaWheels.RotorsChanged = 1;
  if (UnivEnigmaWheels.LeverStepping)
  {
    EnigmaLeverStep();
  }
  else
  {
    EnigmaGearStep();
  }
}


//be sure to call EnigmaStep() prior to calling this function.
//doing this allows multiple keys to be pressed simultaneously
signed char EnigmaEncode(byte pletter)     /*xls*/
{
  static byte state = 0;
  static signed char retvalue = -1;
  static signed char path = 0;
  static byte tletter = 0;
  static byte letter = 0;

  if (pletter == 255)
  {
    state = 0;
    return 0;
  }

  if ((UnivEnigmaWheels.RotorsChanged) && (state > 1))
  {
    TestPlug(255, 0); //reset test plug state machine
    state = 1;
  }

  switch (state)
  {
    case 0:
      {
        if (pletter != 0)
        {
          state = 1;
          tletter = pletter;
          retvalue = -1;
          TestPlug(255, 0); //reset test plug state machine
          printstep(F(""), tletter);
        }
        break;
      }
    // ->plugboard
    case 1:
      {
        UnivEnigmaWheels.RotorsChanged = 0;

        if (UnivEnigmaWheels.NeedsInit == 1)
        {
          UpdateWheelData();
        }
        else
        {
          letter = tletter;
          if (UnivEnigmaWheels.Plugboard)
          {
            if (UnivEnigmaWheels.UseSWPlugs)
            {
              path = UnivEnigmaWheels.EffectivePlugs[letter - 1];
            }
            else
            {
              path = TestPlug(letter, 0); // uhr switch (if present) will do a top to bottom lookup
            }

            if (path != -1)
            {
              state = 3;
              letter = path;
              printstep(F("Stecker"), letter);
            }
          }
          else
          {
            state = 3;
          }
        }
        break;
      }
    // -> dummy state
    case 2:
      {
        state = 3;
        break;
      }
    // ->ETW
    case 3:
      {
        //BUG: enable after rotors are working
        letter = lookuprotorfwd(UnivEnigmaWheels.ETW, 'A', 1, letter);
        printstep(F("ETW"), letter);
        state = 4;
        break;
      }
    // ->rotor1
    case 4:
      {
        letter = lookuprotorfwd(UnivEnigmaWheels.Wheels[0], UnivEnigmaData.RotorPosition[0], UnivEnigmaData.RingSetting[0], letter);
        printstep(F("1"), letter);
        state = 5;
        break;
      }
    // ->rotor2
    case 5:
      {
        letter = lookuprotorfwd(UnivEnigmaWheels.Wheels[1], UnivEnigmaData.RotorPosition[1], UnivEnigmaData.RingSetting[1], letter);
        printstep(F("2"), letter);
        state = 6;
        break;
      }
    // ->rotor3
    case 6:
      {
        letter = lookuprotorfwd(UnivEnigmaWheels.Wheels[2], UnivEnigmaData.RotorPosition[2], UnivEnigmaData.RingSetting[2], letter);
        printstep(F("3"), letter);
        state = 7;
        break;
      }
    // ->rotor4 (if applicable)
    case 7:
      {
        // for [D] [K] [R] [T] [A-865] [G-111] [G-260] [G-312] treat the UKW as a reflector in position 4
        if (UnivEnigmaWheels.SettableUWK)
        {
          letter = lookuprotorfwd(UnivEnigmaWheels.UKW, UnivEnigmaData.RotorPosition[3], UnivEnigmaData.RingSetting[3], letter);
          printstep(F("UKW"), letter);
          state = 10; //and jump to rotor3 backwards
        }
        else
        {
          if (UnivEnigmaData.RotorType[3] != 0)
          {
            letter = lookuprotorfwd(UnivEnigmaWheels.Wheels[3], UnivEnigmaData.RotorPosition[3], UnivEnigmaData.RingSetting[3], letter);
            printstep(F("4"), letter);
          }
          state = 8;
        }
        break;
      }
    // ->UKW
    case 8:
      {
        //M3 M4 or UWKD
        letter = lookupukw(letter);
        printstep(F("UKW"), letter);
        state = 9;
        break;
      }
    // ->rotor4
    case 9:
      {
        if (UnivEnigmaData.RotorType[3] != 0)
        {
          letter = lookuprotorbwd(UnivEnigmaWheels.Wheels[3], UnivEnigmaData.RotorPosition[3], UnivEnigmaData.RingSetting[3], letter);
          printstep(F("4"), letter);
        }
        state = 10;
        break;
      }
    // ->rotor3
    case 10:
      {
        letter = lookuprotorbwd(UnivEnigmaWheels.Wheels[2], UnivEnigmaData.RotorPosition[2], UnivEnigmaData.RingSetting[2], letter);
        printstep(F("3"), letter);
        state = 11;
        break;
      }
    // ->rotor2
    case 11:
      {
        letter = lookuprotorbwd(UnivEnigmaWheels.Wheels[1], UnivEnigmaData.RotorPosition[1], UnivEnigmaData.RingSetting[1], letter);
        printstep(F("2"), letter);
        state = 12;
        break;
      }
    // ->rotor1
    case 12:
      {
        letter = lookuprotorbwd(UnivEnigmaWheels.Wheels[0], UnivEnigmaData.RotorPosition[0], UnivEnigmaData.RingSetting[0], letter);
        printstep(F("1"), letter);
        state = 13;
        break;
      }
    // ->etw
    case 13:
      {
        letter = lookuprotorbwd(UnivEnigmaWheels.ETW, 'A', 1, letter);
        printstep(F("ETW"), letter);
        state = 16;
        break;
      }
    // -> dummy state
    case 14:
      {
        state = 16;
        break;
      }
    //dummy state
    case 15:
      {
        state = 16;
        break;
      }
    // ->plugboard
    case 16:
      {
        if (UnivEnigmaWheels.Plugboard)
        {
          if (UnivEnigmaWheels.UseSWPlugs)
          {
            for (byte i = 0; i < 26; i++)
            {
              if (UnivEnigmaWheels.EffectivePlugs[i] == letter)
              {
                path = i + 1;
              }
            }
          }
          else
          {
            path = TestPlug(letter, 1); // if uhr is connected, perform a bottom to top lookup
          }

          if (path != -1)
          {
            state = 0;
            letter = path;
            retvalue = letter;
            printstep(F("Stecker"), letter);
            //Serial.print(F("\x0d\x0a")); //BUG: disable if printer is not enabled
          }
        }
        else
        {
          state = 0;
          retvalue = letter;
        }
        break;
      }
  }

  return retvalue;
}
