// MegaEnigma.ino - Plugboard.ino
// @arduinoenigma 2019


//
byte writePlug(byte plug, byte pval)     /*xls*/
{
  switch (plug)
  {
    case 1:
      {
        if (pval)
        {
          PlugA.input();
          PlugA.high();
        }
        else
        {
          PlugA.low();
          PlugA.output();
        }
        break;
      }
    case 2:
      {
        if (pval)
        {
          PlugB.input();
          PlugB.high();
        }
        else
        {
          PlugB.low();
          PlugB.output();
        }
        break;
      }

    case 3:
      {
        if (pval)
        {
          PlugC.input();
          PlugC.high();
        }
        else
        {
          PlugC.low();
          PlugC.output();
        }
        break;
      }
    case 4:
      {
        if (pval)
        {
          PlugD.input();
          PlugD.high();
        }
        else
        {
          PlugD.low();
          PlugD.output();
        }
        break;
      }
    case 5:
      {
        if (pval)
        {
          PlugE.input();
          PlugE.high();
        }
        else
        {
          PlugE.low();
          PlugE.output();
        }
        break;
      }
    case 6:
      {
        if (pval)
        {
          PlugF.input();
          PlugF.high();
        }
        else
        {
          PlugF.low();
          PlugF.output();
        }
        break;
      }
    case 7:
      {
        if (pval)
        {
          PlugG.input();
          PlugG.high();
        }
        else
        {
          PlugG.low();
          PlugG.output();
        }
        break;
      }
    case 8:
      {
        if (pval)
        {
          PlugH.input();
          PlugH.high();
        }
        else
        {
          PlugH.low();
          PlugH.output();
        }
        break;
      }
    case 9:
      {
        if (pval)
        {
          PlugI.input();
          PlugI.high();
        }
        else
        {
          PlugI.low();
          PlugI.output();
        }
        break;
      }
    case 10:
      {
        if (pval)
        {
          PlugJ.input();
          PlugJ.high();
        }
        else
        {
          PlugJ.low();
          PlugJ.output();
        }
        break;
      }
    case 11:
      {
        if (pval)
        {
          PlugK.input();
          PlugK.high();
        }
        else
        {
          PlugK.low();
          PlugK.output();
        }
        break;
      }
    case 12:
      {
        if (pval)
        {
          PlugL.input();
          PlugL.high();
        }
        else
        {
          PlugL.low();
          PlugL.output();
        }
        break;
      }
    case 13:
      {
        if (pval)
        {
          PlugM.input();
          PlugM.high();
        }
        else
        {
          PlugM.low();
          PlugM.output();
        }
        break;
      }
    case 14:
      {
        if (pval)
        {
          PlugN.input();
          PlugN.high();
        }
        else
        {
          PlugN.low();
          PlugN.output();
        }
        break;
      }
    case 15:
      {
        if (pval)
        {
          PlugO.input();
          PlugO.high();
        }
        else
        {
          PlugO.low();
          PlugO.output();
        }
        break;
      }
    case 16:
      {
        if (pval)
        {
          PlugP.input();
          PlugP.high();
        }
        else
        {
          PlugP.low();
          PlugP.output();
        }
        break;
      }
    case 17:
      {
        if (pval)
        {
          PlugQ.input();
          PlugQ.high();
        }
        else
        {
          PlugQ.low();
          PlugQ.output();
        }
        break;
      }
    case 18:
      {
        if (pval)
        {
          PlugR.input();
          PlugR.high();
        }
        else
        {
          PlugR.low();
          PlugR.output();
        }
        break;
      }
    case 19:
      {
        if (pval)
        {
          PlugS.input();
          PlugS.high();
        }
        else
        {
          PlugS.low();
          PlugS.output();
        }
        break;
      }
    case 20:
      {
        if (pval)
        {
          PlugT.input();
          PlugT.high();
        }
        else
        {
          PlugT.low();
          PlugT.output();
        }
        break;
      }
    case 21:
      {
        if (pval)
        {
          PlugU.input();
          PlugU.high();
        }
        else
        {
          PlugU.low();
          PlugU.output();
        }
        break;
      }
    case 22:
      {
        if (pval)
        {
          PlugV.input();
          PlugV.high();
        }
        else
        {
          PlugV.low();
          PlugV.output();
        }
        break;
      }
    case 23:
      {
        if (pval)
        {
          PlugW.input();
          PlugW.high();
        }
        else
        {
          PlugW.low();
          PlugW.output();
        }
        break;
      }
    case 24:
      {
        if (pval)
        {
          PlugX.input();
          PlugX.high();
        }
        else
        {
          PlugX.low();
          PlugX.output();
        }
        break;
      }
    case 25:
      {
        if (pval)
        {
          PlugY.input();
          PlugY.high();
        }
        else
        {
          PlugY.low();
          PlugY.output();
        }
        break;
      }
    case 26:
      {
        if (pval)
        {
          PlugZ.input();
          PlugZ.high();
        }
        else
        {
          PlugZ.low();
          PlugZ.output();
        }
        break;
      }
  }
}


//
byte readPlug(byte plug)     /*xls*/
{
  switch (plug)
  {
    case 1:
      {
        return PlugA.read();  // no need for break, return exits function
      }
    case 2:
      {
        return PlugB.read();
      }
    case 3:
      {
        return PlugC.read();
      }
    case 4:
      {
        return PlugD.read();
      }
    case 5:
      {
        return PlugE.read();
      }
    case 6:
      {
        return PlugF.read();
      }
    case 7:
      {
        return PlugG.read();
      }
    case 8:
      {
        return PlugH.read();
      }
    case 9:
      {
        return PlugI.read();
      }
    case 10:
      {
        return PlugJ.read();
      }
    case 11:
      {
        return PlugK.read();
      }
    case 12:
      {
        return PlugL.read();
      }
    case 13:
      {
        return PlugM.read();
      }
    case 14:
      {
        return PlugN.read();
      }
    case 15:
      {
        return PlugO.read();
      }
    case 16:
      {
        return PlugP.read();
      }
    case 17:
      {
        return PlugQ.read();
      }
    case 18:
      {
        return PlugR.read();
      }
    case 19:
      {
        return PlugS.read();
      }
    case 20:
      {
        return PlugT.read();
      }
    case 21:
      {
        return PlugU.read();
      }
    case 22:
      {
        return PlugV.read();
      }
    case 23:
      {
        return PlugW.read();
      }
    case 24:
      {
        return PlugX.read();
      }
    case 25:
      {
        return PlugY.read();
      }
    case 26:
      {
        return PlugZ.read();
      }

    default:
      {
        return 1;
      }
  }
}


// a state machine that scans a plug and returns -1 (scan in progress), or value of other end of plug or itself if no plug installed
// topbot is a parameter needed for an external uhr switch which replies with two plugs in response to our plug,
// top==1 returns first response from uhr switch
// top==2 returns second response from uhr switch
// if a physical plug or nothing is installed, either value of top returns same result
signed char TestPlug(byte letter, byte topbot)     /*xls*/
{
  static byte state = 0;

  static byte sourceplug = 0;
  static byte destinationplug = 0;
  static byte topfound = 0;
  static byte botfound = 0;
  static signed char plugfound = -1;
  static signed char  retvalue = -1;
  static unsigned long entryt;

  //Serial.print(F("s:"));
  //Serial.print(state);
  //Serial.print(F(" src:"));
  //Serial.print(sourceplug);
  //Serial.print(F(" dst:"));
  //Serial.print(destinationplug);
  //Serial.print(F(" fnd:"));
  //Serial.println(plugfound);

  entrytimer(TASKSINGLEPLUG);

  if (letter == 255)
  {
    state = 0;
    allPlugsInput();
    return 0;
  }

  //BUG: to tune:
  // set to t > 150000
  // then slowly increase in UHRSwitchlogic.ino etimer until with default enigma AAPP = FCAB
  // then double etimer
  // then decrease t> until it fails
  // then double t>

  unsigned long t = micros() - entryt;
  if ((state != 0) && (t > 20000)) //45000
  {
    state = 4;
  }

  switch (state)
  {
    case 0:
      if (letter != 0)
      {
        state = 1;
        sourceplug = letter;
        destinationplug = 0;
        topfound = 0;
        botfound = 0;
        plugfound = -1;
        retvalue = -1;
        entryt = micros();
        allPlugsInput();
      }
      break;
    case 1:
      {
        writePlug(sourceplug, 0);
        state = 2;
        break;
      }
    case 2:
      {
        destinationplug++;
        if (destinationplug == sourceplug)
        {
          destinationplug++;
        }

        if (destinationplug > 26) // two succesive increments can push us over 26
        {
          destinationplug = 1;
        }

        if (readPlug(destinationplug) == 0)
        {
          if (topfound == 0)
          {
            topfound = destinationplug;
            state = 3;
          }
          else
          {
            botfound = destinationplug;
            state = 4;
          }
        }

        break;
      }
    case 3:
      {
        if (readPlug(destinationplug))
        {
          state = 2;
        }

        break;
      }

    case 4:
      {
        allPlugsInput();
        state = 0;

        if (topbot == 0)
        {
          if (topfound != 0)
          {
            retvalue = topfound;           // top UHR plug or wired plug found
          }
          else
          {
            retvalue = sourceplug;         // nothing found, self steckered
          }
        }
        else
        {
          if (botfound != 0)
          {
            retvalue = botfound;          // bot UHR plug found (wired plug never gets here)
          }
          else
          {
            if (topfound != 0)
            {
              retvalue = topfound;       // wired plug found
            }
            else
            {
              retvalue = sourceplug;     // nothing found, self steckered
            }
          }
        }

        break;
      }
  }

  exittimer(TASKSINGLEPLUG);

  return retvalue;
}
