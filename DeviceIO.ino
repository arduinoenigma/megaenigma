// MegaEnigma.ino - DeviceIO.ino
// @arduinoenigma 2019

#if defined(CommonCathode)
#define SegmentOn high
#define SegmentOff low
#define DigitOn low
#define DigitOff high
#define LampsOff high
#define LampsOn low
#endif

#if defined(CommonAnode)
#define SegmentOn low
#define SegmentOff high
#define DigitOn high
#define DigitOff low
#define LampsOff high
#define LampsOn low
#endif

GPIO<BOARD::D2>  SegmentA2;
GPIO<BOARD::D3>  SegmentA1;
GPIO<BOARD::D4>  SegmentK;
GPIO<BOARD::D5>  SegmentJ;
GPIO<BOARD::D6>  SegmentB;
GPIO<BOARD::D7>  SegmentH;
GPIO<BOARD::D8>  SegmentG2;
GPIO<BOARD::D9>  SegmentF;
GPIO<BOARD::D10> SegmentL;
GPIO<BOARD::D11> SegmentG1;
GPIO<BOARD::D12> SegmentC;
GPIO<BOARD::D13> SegmentM;
GPIO<BOARD::D14> SegmentDP;
GPIO<BOARD::D15> SegmentN;
GPIO<BOARD::D16> CCDS4;
GPIO<BOARD::D17> SegmentE;
GPIO<BOARD::D18> SegmentD2;
GPIO<BOARD::D19> SegmentD1;
GPIO<BOARD::D20> CCDS2;
GPIO<BOARD::D21> CCDS3;
GPIO<BOARD::D22> CCDS1;
GPIO<BOARD::D23> KEY1L;
GPIO<BOARD::D24> LED2L;
GPIO<BOARD::D25> KEY2L;
GPIO<BOARD::D26> PlugI;
GPIO<BOARD::D27> LED1L;
GPIO<BOARD::D28> PlugM;
GPIO<BOARD::D29> PlugK;
GPIO<BOARD::D30> PlugL;
GPIO<BOARD::D31> PlugO;
GPIO<BOARD::D32> PlugN;
GPIO<BOARD::D33> PlugJ;
GPIO<BOARD::D34> PlugH;
GPIO<BOARD::D35> PlugU;
GPIO<BOARD::D36> PlugZ;
GPIO<BOARD::D37> PlugB;
GPIO<BOARD::D38> PlugV;
GPIO<BOARD::D39> PlugG;
GPIO<BOARD::D40> PlugQ;
GPIO<BOARD::D41> PlugT;
GPIO<BOARD::D42> PlugA;
GPIO<BOARD::D43> PlugP;
GPIO<BOARD::D44> PlugW;
GPIO<BOARD::D45> PlugS;
GPIO<BOARD::D46> UPDNL;
GPIO<BOARD::D47> PlugY;
GPIO<BOARD::D48> PlugX;
GPIO<BOARD::D49> PlugE;
GPIO<BOARD::D50> PlugR;
GPIO<BOARD::D51> PlugD;
GPIO<BOARD::D52> PlugF;
GPIO<BOARD::D53> PlugC;


void allSegmentOutput()     /*xls*/
{
  SegmentA1.output();
  SegmentA2.output();
  SegmentB.output();
  SegmentC.output();
  SegmentD1.output();
  SegmentD2.output();
  SegmentDP.output();
  SegmentE.output();
  SegmentF.output();
  SegmentG1.output();
  SegmentG2.output();
  SegmentH.output();
  SegmentJ.output();
  SegmentK.output();
  SegmentL.output();
  SegmentM.output();
  SegmentN.output();
}


void allSegmentInput()     /*xls*/
{
  SegmentA1.input();
  SegmentA2.input();
  SegmentB.input();
  SegmentC.input();
  SegmentD1.input();
  SegmentD2.input();
  SegmentDP.input();
  SegmentE.input();
  SegmentF.input();
  SegmentG1.input();
  SegmentG2.input();
  SegmentH.input();
  SegmentJ.input();
  SegmentK.input();
  SegmentL.input();
  SegmentM.input();
  SegmentN.input();
}

// used in display context to turn off all segments
void allSegmentOff()     /*xls*/
{
  SegmentA1.SegmentOff();
  SegmentA2.SegmentOff();
  SegmentB.SegmentOff();
  SegmentC.SegmentOff();
  SegmentD1.SegmentOff();
  SegmentD2.SegmentOff();
  SegmentDP.SegmentOff();
  SegmentE.SegmentOff();
  SegmentF.SegmentOff();
  SegmentG1.SegmentOff();
  SegmentG2.SegmentOff();
  SegmentH.SegmentOff();
  SegmentJ.SegmentOff();
  SegmentK.SegmentOff();
  SegmentL.SegmentOff();
  SegmentM.SegmentOff();
  SegmentN.SegmentOff();
}


// used in key reading context to set all the segments to high
void allSegmentHigh()     /*xls*/
{
  SegmentA1.high();
  SegmentA2.high();
  SegmentB.high();
  SegmentC.high();
  SegmentD1.high();
  SegmentD2.high();
  SegmentDP.high();
  SegmentE.high();
  SegmentF.high();
  SegmentG1.high();
  SegmentG2.high();
  SegmentH.high();
  SegmentJ.high();
  SegmentK.high();
  SegmentL.high();
  SegmentM.high();
  SegmentN.high();
}


// used in lampfield displaying context to set all the segments to low
void allSegmentLow()     /*xls*/
{
  SegmentA1.low();
  SegmentA2.low();
  SegmentB.low();
  SegmentC.low();
  SegmentD1.low();
  SegmentD2.low();
  SegmentDP.low();
  SegmentE.low();
  SegmentF.low();
  SegmentG1.low();
  SegmentG2.low();
  SegmentH.low();
  SegmentJ.low();
  SegmentK.low();
  SegmentL.low();
  SegmentM.low();
  SegmentN.low();
}


void allDigitsOutput()     /*xls*/
{
  CCDS1.output();
  CCDS2.output();
  CCDS3.output();
  CCDS4.output();
}


void allDigitsInput()     /*xls*/
{
  CCDS1.input();
  CCDS2.input();
  CCDS3.input();
  CCDS4.input();
}


void allDigitsOff()     /*xls*/
{
  CCDS1.DigitOff();
  CCDS2.DigitOff();
  CCDS3.DigitOff();
  CCDS4.DigitOff();
}


void allLampsOutput()     /*xls*/
{
  LED1L.output();
  LED2L.output();
}


void allLampsInput()     /*xls*/
{
  LED1L.input();
  LED2L.input();
}


void allLampsOff()     /*xls*/
{
  LED1L.LampsOff();
  LED2L.LampsOff();
}


void allKeysInput()     /*xls*/
{
  KEY1L.input();
  KEY1L.high();

  KEY2L.input();
  KEY2L.high();

  UPDNL.input();
  UPDNL.high();
}


void allPlugsInput()     /*xls*/
{
  PlugA.input();
  PlugB.input();
  PlugC.input();
  PlugD.input();
  PlugE.input();
  PlugF.input();
  PlugG.input();
  PlugH.input();
  PlugI.input();
  PlugJ.input();
  PlugK.input();
  PlugL.input();
  PlugM.input();
  PlugN.input();
  PlugO.input();
  PlugP.input();
  PlugQ.input();
  PlugR.input();
  PlugS.input();
  PlugT.input();
  PlugU.input();
  PlugV.input();
  PlugW.input();
  PlugX.input();
  PlugY.input();
  PlugZ.input();

  PlugA.high();
  PlugB.high();
  PlugC.high();
  PlugD.high();
  PlugE.high();
  PlugF.high();
  PlugG.high();
  PlugH.high();
  PlugI.high();
  PlugJ.high();
  PlugK.high();
  PlugL.high();
  PlugM.high();
  PlugN.high();
  PlugO.high();
  PlugP.high();
  PlugQ.high();
  PlugR.high();
  PlugS.high();
  PlugT.high();
  PlugU.high();
  PlugV.high();
  PlugW.high();
  PlugX.high();
  PlugY.high();
  PlugZ.high();
}
