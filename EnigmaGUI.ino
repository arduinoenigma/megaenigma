// MegaEnigma.ino - EnigmaGUI.ino
// @arduinoenigma 2019

/*
  MENU:

  MACH
   I--A
   I--B
   I--C
   M3-B
   M3-C
   M3-D
   M3D1
   M3D2
   M3D3
   M4-B
   M4-C
   N---
   S---
   D---
   K---
   R---
   T---
   KD-K
   A865
   G111
   G260
   G312
  UKWD
   bpAF
   udAV
  ROTR
   B321
   G843
  RING
   AAAA
  PLUG
   ----
    1AB
  -UHR
   --39
  -V12

*/

struct menuData_t     /*xls*/
{
  int  InhibitMenuTimer = 0;

  byte Menu = 0;
  byte SubChange = 0;
  byte StayInThisMenu = 0;
  signed char SubMenu[7] = {0, 0, 0, 0, 0, 0, 0}; //WARNING, make sure size matches number of menus or you will create a BUG
  byte UDBP = 0;
  byte UKWSocket = 1;
  byte UKWPlugChanged = 0;
  byte InhibitKeyboard = 0;
  byte RotorsChanged = 0;
  byte KeyPressed = 0;

  byte LastUDPlug = 0;

  byte CurrentSWPLug = 0;
  byte SWPlugLeft = 0;
  byte SWPlugRight = 0;

  byte SWPairChanged = 0;
  byte SWPlugChanged = 0;

  byte LastSelectedRotor = 0;

  byte LastRingIndex = 0;
  byte LastRingCommand = 0;

  byte LastPlugLR = 0;

  //allows multiple key presses, stores result of encoding letters A..Z
  byte EnigmaResults[26] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};      /*xls*/

  byte PlugsFound[26] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
}
MenuData;


//
void ShowRotorPositions()     /*xls*/
{
  for (byte i = 0; i < 4; i++)
  {
    NewDisplayValues[i] = GetRotorPosition(3 - i) - 'A' + 1; // i:0123 -> RotorIndex:3210
    DisplayValues[i] = NewDisplayValues[i];
  }
  updateRawValues();
}


//
void SetInitialMachineID(byte pvalue)   /*xls*/
{
  MenuData.SubMenu[0] = pvalue;
}


//BUG: need a function that translates model into calls to: byte SetUKW(byte pmachine, byte pukw)
//BUG: D reflector 1,2,3 may belong to M3 instead of KD, renumber models below:
//BUG: write Set Methods for MachineType,UKW,RotorType,Ring,Position
//BUG: verify M3 D reflector is M3M3
//
void DisplayModel(byte model, byte init)   /*xls*/
{
  if (init)
  {
    SetRotorType(0, 1);                   // right rotor is I
    SetRotorType(1, 2);                   // mid rotor is II
    SetRotorType(2, 3);                   // left rotor is III
    SetRotorType(3, 0);                   // leftmost rotor is 0

    SetRingSetting(0, 1);                // BUG: 1 for normal use
    SetRingSetting(1, 1);
    SetRingSetting(2, 1);
    SetRingSetting(3, 1);

    SetRotorPosition(0, 'A');
    SetRotorPosition(1, 'A');
    SetRotorPosition(2, 'A');
    SetRotorPosition(3, 0);

    MenuData.SubMenu[0] = model;         // make selected model and menu match
  }

  switch (model)
  {
    // if model is out of range, select enigma I
    default:
    case 0:
      {
        Display4(F("I**A"));
        if (init)
        {
          SetMachineType(1);
          SetUKWType(1);
        }
        break;
      }
    case 1:
      {
        Display4(F("I**B"));
        if (init)
        {
          SetMachineType(1);
          SetUKWType(2);
        }
        break;
      }
    case 2:
      {
        Display4(F("I**C"));
        if (init)
        {
          SetMachineType(1);
          SetUKWType(3);
        }
        break;
      }
    case 3:
      {
        Display4(F("M3*B"));
        if (init)
        {
          SetMachineType(2);
          SetUKWType(2);
        }
        break;
      }
    case 4:
      {
        Display4(F("M3*C"));
        if (init)
        {
          SetMachineType(2);
          SetUKWType(3);
        }
        break;
      }
    case 5:
      {
        Display4(F("M3*D"));
        if (init)
        {
          SetMachineType(2);
          SetUKWType(6);
        }
        break;
      }
    case 6:
      {
        Display4(F("M3D1"));
        if (init)
        {
          SetMachineType(2);
          SetUKWType(7);
        }
        break;
      }
    case 7:
      {
        Display4(F("M3D2"));
        if (init)
        {
          SetMachineType(2);
          SetUKWType(8);
        }
        break;
      }
    case 8:
      {
        Display4(F("M3D3"));
        if (init)
        {
          SetMachineType(2);
          SetUKWType(9);
        }
        break;
      }
    case 9:
      {
        Display4(F("M4*B"));
        if (init)
        {
          SetMachineType(3);
          SetUKWType(4);
          SetRotorType(3, 9);                   // leftmost rotor is B
          SetRotorPosition(3, 'A');
        }
        break;
      }
    case 10:
      {
        Display4(F("M4*C"));
        if (init)
        {
          SetMachineType(3);
          SetUKWType(5);
          SetRotorType(3, 10);                   // leftmost rotor is G
          SetRotorPosition(3, 'A');
        }
        break;
      }
    case 11:
      {
        Display4(F("N***"));
        if (init)
        {
          SetMachineType(4);
          SetUKWType(0);
        }
        break;
      }
    case 12:
      {
        Display4(F("S***"));
        if (init)
        {
          SetMachineType(5);
          SetUKWType(0);
        }
        break;
      }
    case 13:
      {
        Display4(F("D***"));
        if (init)
        {
          SetMachineType(6);
          SetUKWType(0);
          SetRotorType(3, 0);                   // leftmost rotor
          SetRotorPosition(3, 'A');
        }
        break;
      }
    case 14:
      {
        Display4(F("K***"));
        if (init)
        {
          SetMachineType(7);
          SetUKWType(0);
          SetRotorType(3, 0);                   // leftmost rotor
          SetRotorPosition(3, 'A');
        }
        break;
      }
    case 15:
      {
        Display4(F("R***"));
        if (init)
        {
          SetMachineType(8);
          SetUKWType(0);
          SetRotorType(3, 0);                   // leftmost rotor
          SetRotorPosition(3, 'A');
        }
        break;
      }
    case 16:
      {
        Display4(F("T***"));
        if (init)
        {
          SetMachineType(9);
          SetUKWType(0);
          SetRotorType(3, 0);                   // leftmost rotor
          SetRotorPosition(3, 'A');
        }
        break;
      }
    case 17:
      {
        Display4(F("KD*K"));
        if (init)
        {
          SetMachineType(10);
          SetUKWType(1);
        }
        break;
      }
    case 18:
      {
        Display4(F("A865"));
        if (init)
        {
          SetMachineType(11);
          SetUKWType(0);
          SetRotorType(3, 0);                   // leftmost rotor
          SetRotorPosition(3, 'A');
        }
        break;
      }
    case 19:
      {
        Display4(F("G111"));
        if (init)
        {
          SetMachineType(12);
          SetUKWType(0);
          SetRotorType(3, 0);                   // leftmost rotor
          SetRotorPosition(3, 'A');
        }
        break;
      }
    case 20:
      {
        Display4(F("G260"));
        if (init)
        {
          SetMachineType(13);
          SetUKWType(0);
          SetRotorType(3, 0);                   // leftmost rotor
          SetRotorPosition(3, 'A');
        }
        break;
      }
    case 21:
      {
        Display4(F("G312"));
        if (init)
        {
          SetMachineType(14);
          SetUKWType(0);
          SetRotorType(3, 0);                   // leftmost rotor
          SetRotorPosition(3, 'A');
        }
        break;
      }
  }

  if (init)
  {
    // call this to initialize the machine and copy rotor definitions into UnivEnigmaWheels.
    UpdateWheelData();
    ActivateUKWD();
    InitSWPlugs();
    EnigmaCheckWheelLocked();
  }

  //dbgPrintEnigma(); // BUG: remove
}


//
void ShowMenu(const __FlashStringHelper * pWord)    /*xls*/
{
  allLightsOff();
  Display4(pWord, false);
  LampFieldMessage(pWord);
}


//
void ShowPlugs(byte enable)   /*xls*/
{
  static byte enabled;
  static byte plugscan = 1;

  if (enable != 255)
  {
    enabled = enable;

    for (byte i = 0; i < 26; i++)
    {
      MenuData.PlugsFound[i] = 0;
    }

    if (enabled == 0)
    {
      allPlugsInput();
    }
  }

  if (enabled == 1)
  {
    signed char plugr = TestPlug(plugscan, 0);
    if (plugr != -1)
    {
      if (plugr != plugscan)
      {
        lightOn(plugr);
        lightOn(plugscan);

        MenuData.PlugsFound[plugr - 1] = plugscan;
        MenuData.PlugsFound[plugscan - 1] = plugr;
      }
      else
      {
        lightOff(plugr);
        lightOff(MenuData.PlugsFound[plugr - 1]);

        MenuData.PlugsFound[plugr - 1] = 0;
      }
      plugscan++;
      if (plugscan > 26)
      {
        plugscan = 1;
      }
    }
  }
}


//
void GoToMachine(byte first, byte last, byte key)     /*xls*/
{
  static byte lastkey = 255;

  if (key != 255)
  {
    if ((key != lastkey) && (MenuData.SubMenu[MenuData.Menu - 1] != first))
    {
      MenuData.SubMenu[MenuData.Menu - 1] = first;
    }
    else
    {
      if (MenuData.SubMenu[MenuData.Menu - 1] < last)
      {
        MenuData.SubMenu[MenuData.Menu - 1]++;
      }
      else
      {
        MenuData.SubMenu[MenuData.Menu - 1] = first;
      }
    }
  }

  lastkey = key;
}


//BUG: first time on power up menu key skips RING
//
//byte doMenu(byte menu, signed char submenu, byte subchange, byte key)     /*xls*/
byte doMenu(byte key)     /*xls*/
{
  byte exitmenu = 0;

  //0123  index
  //1234  down keys
  //abcd  up keys

  //5     menu
  //A..Z  65..90

  switch (key)
  {
    case '1':
    case 'a':
      {
        MenuData.LastSelectedRotor = 0;
        break;
      }

    case '2':
    case 'b':
      {
        MenuData.LastSelectedRotor = 1;
        break;
      }

    case '3':
    case 'c':
      {
        MenuData.LastSelectedRotor = 2;
        break;
      }

    case '4':
    case 'd':
      {
        MenuData.LastSelectedRotor = 3;
        break;
      }
  }

  switch (MenuData.Menu)
  {
    case 1:
      {
        byte oldsubchange = MenuData.SubChange; // to stay in MACH display if unasigned key like F is pushed

        if ((key > 'A' - 1) && (key < 'Z' + 1))
        {
          MenuData.SubChange = 2;
        }

        if (MenuData.SubChange == 0)
        {
          ShowMenu(F("MACH"));
          GoToMachine(0, 0, 255); // reset lastkey
        }
        else
        {
          switch (key)
          {
            case 'I':
              {
                GoToMachine(0, 2, key);
                break;
              }
            case 'M':
              {
                GoToMachine(3, 10, key);
                break;
              }
            case 'N':
              {
                GoToMachine(11, 11, key);
                break;
              }
            case 'S':
              {
                GoToMachine(12, 12, key);
                break;
              }
            case 'D':
              {
                GoToMachine(13, 13, key);
                break;
              }
            case 'K':
              {
                GoToMachine(14, 15, key);
                if (MenuData.SubMenu[MenuData.Menu - 1] == 15) // K and KD are not contiguous
                {
                  MenuData.SubMenu[MenuData.Menu - 1] = 17;
                }
                break;
              }
            case 'R':
              {
                GoToMachine(15, 15, key);
                break;
              }
            case 'T':
              {
                GoToMachine(16, 16, key);
                break;
              }
            case 'A':
              {
                GoToMachine(18, 18, key);
                break;
              }
            case 'G':
              {
                GoToMachine(19, 21, key);
                break;
              }
            default:
              {
                MenuData.SubChange = oldsubchange;
                GoToMachine(0, 0, 255); // reset lastkey if a scroll key is used
              }
          }

          if (MenuData.SubChange != 0) // we may have entered here, but changed SubChange back to 0 if unasigned key
          {
            if (MenuData.SubMenu[MenuData.Menu - 1] > 21)
            {
              MenuData.SubMenu[MenuData.Menu - 1] = 0;
            }

            if (MenuData.SubMenu[MenuData.Menu - 1] < 0)
            {
              MenuData.SubMenu[MenuData.Menu - 1] = 21;
            }

            DisplayModel(MenuData.SubMenu[MenuData.Menu - 1], MenuData.SubChange == 2);

            if (MenuData.SubChange == 2)
            {
              SetMachineID(MenuData.SubMenu[MenuData.Menu - 1]);
            }
          }

        }

        break;
      }

    case 2:
      {
        if (HasUKWD())
        {
          if ((MenuData.SubChange == 1) && (key > 'A' - 1) && (key < 'Z' + 1))
          {
            MenuData.SubChange = 2;
          }

          if (MenuData.SubChange == 0)
          {
            ShowMenu(F("UKWD"));
            MenuData.UDBP = 0;
            MenuData.UKWSocket = 1;
            MenuData.UKWPlugChanged = 0;
          }
          else
          {
            byte ukwdtries = 0;
            byte goodvalue;
            byte v;

            do
            {
              if (MenuData.SubChange == 2)
              {
                //Serial.print(F("S: "));  Serial.print(RAMEND - SP); //BUG: disable, does not seem to affect stack size

                //3210  index
                //1234  down keys
                //abcd  up keys

                switch (key)
                {
                  case 'a':
                  case 'b':
                  case '1':
                  case '2':
                    {
                      if (MenuData.UDBP)
                      {
                        MenuData.UDBP = 0;
                      }
                      else
                      {
                        MenuData.UDBP = 1;
                      }
                      break;
                    }
                  case '3':
                  case 'c':
                    {
                      if (MenuData.UKWPlugChanged)
                      {
                        if (MenuData.UDBP)
                        {
                          //save the complementary pais in UD mode...
                          changeukw(UDtoBP(DisplayValues[3]) - 'A', UDtoBP(DisplayValues[2]) - 'A' + 'a');
                        }
                        else
                        {
                          //save the complementary pais in BP mode...
                          changeukw(lookupukw(MenuData.UKWSocket) - 1, MenuData.UKWSocket + 'a' - 1); // before changing current pair, save (A-F) to (F-A)
                        }

                        MenuData.UKWPlugChanged = 0;
                        MenuData.StayInThisMenu = checkUKWD();
                      }

                      if (key == '3')
                      {
                        MenuData.UKWSocket--;
                        if (MenuData.UKWSocket == 0)
                        {
                          MenuData.UKWSocket = 26;
                        }
                      }
                      else
                        //if (key == 'c')
                      {
                        MenuData.UKWSocket++;
                        if (MenuData.UKWSocket == 27)
                        {
                          MenuData.UKWSocket = 1;
                        }
                      }

                      break;
                    }

                  case '4':
                  case 'd':
                  default:
                    {
                      //do not allow up/down to edit fixed pairs
                      //BO / OB in BP notation
                      //JY / YJ in UD notation
                      if (((MenuData.UDBP == 0) && ((MenuData.UKWSocket != 2) && (MenuData.UKWSocket != 15))) ||
                          ((MenuData.UDBP == 1) && ((MenuData.UKWSocket != 10) && (MenuData.UKWSocket != 25))))
                      {
                        MenuData.StayInThisMenu = 1;

                        if ((MenuData.UKWPlugChanged) && (MenuData.UDBP))
                        {
                          v = MenuData.LastUDPlug;
                        }
                        else
                        {
                          if (MenuData.UDBP == 1)
                          {
                            v = translateBPtoUD(MenuData.UKWSocket);
                            //v = BPtoUD(v) - 'A' + 1;
                            v = UDtoBP(v) - 'A' + 1;
                          }
                          else
                          {
                            v = lookupukw(MenuData.UKWSocket);
                          }
                        }

                        MenuData.UKWPlugChanged = 1;

                        if (((MenuData.UDBP == 0) && (key == '4')) || ((MenuData.UDBP == 1) && (key == 'd')))
                        {
                          v--;
                        }

                        if (((MenuData.UDBP == 0) && (key == 'd')) || ((MenuData.UDBP == 1) && (key == '4')))
                        {
                          v++;
                        }

                        if ((key != 'd') && (key != '4'))
                        {
                          if ((MenuData.UDBP == 0) && ((key - ('A' - 1)) != MenuData.UKWSocket))
                          {
                            v = key - ('A' - 1);
                          }

                        }

                        if (v == 0)
                        {
                          v = 26;
                        }
                        if (v > 26)
                        {
                          v = 1;
                        }

                        if (MenuData.UDBP)
                        {
                          //UD
                          MenuData.LastUDPlug = v;
                          changeukw(UDtoBP(MenuData.UKWSocket) - 'A', v + 'a' - 1);
                        }
                        else
                        {
                          //BP
                          changeukw(MenuData.UKWSocket - 1, v + 'a' - 1);
                        }
                      }

                      break;
                    }
                }
              }

              if (MenuData.UDBP)
              {
                Display4(F("ud**"), false);
              }
              else
              {
                Display4(F("bp**"), false);
              }

              NewDisplayValues[2] = MenuData.UKWSocket;
              DisplayValues[2] = MenuData.UKWSocket;

              if (MenuData.UDBP == 1)
              {
                NewDisplayValues[3] = translateBPtoUD(MenuData.UKWSocket);
              }
              else
              {
                NewDisplayValues[3] = lookupukw(MenuData.UKWSocket);
              }

              DisplayValues[3] = NewDisplayValues[3];

              //check to see it up/down key has gotten us to a bad case
              //since we can get here via up or down, loop back to the switch statement and redo the action (key up or key dn) that got us here
              // bad cases:
              // cannot set a plug to itself A-A
              // cannot set a plug to nonsense A-?
              // in BP mode, cannot set a plug to B or O (A-O) (A-B), but it can display it (B-O) (O-B)
              // in UD mode, cannot set a plug to J or Y (A-J) (A-Y), but it can display it (J-Y) (Y-J)
              if ( (DisplayValues[2] == DisplayValues[3]) ||
                   (DisplayValues[3] > 26) ||
                   ((MenuData.UDBP == 0) && (DisplayValues[2] != 2) && (DisplayValues[2] != 15) && ((DisplayValues[3] == 2) || (DisplayValues[3] == 15))) ||
                   ((MenuData.UDBP == 1) && (DisplayValues[2] != 10) && (DisplayValues[2] != 25) && ((DisplayValues[3] == 10) || (DisplayValues[3] == 25))) )
              {
                goodvalue = 0;
              }
              else
              {
                goodvalue = 1;
              }

              ukwdtries++;

            } while ((goodvalue == 0) && (ukwdtries < 4));

            updateRawValues();
          }

          if (!((key == '5') && (MenuData.UKWPlugChanged)))
          {
            break; // put break here to stay in menu 2, otherwise fall through to 3
          }
        }
        else
        {
          MenuData.Menu++;
          // fall through to case 3, no break;
        }

        //BUG: enter UKWD menu in bp mode, set A to F, move to B, go back to A and set to G, push menu, hangs (now A disappears)

        //allow exiting this menu when everything is correct and menu is pushed
        if ((key == '5') && (MenuData.UKWPlugChanged))
        {
          if (MenuData.UDBP)
          {
            //save the complementary pais in UD mode...
            changeukw(UDtoBP(DisplayValues[3]) - 'A', UDtoBP(DisplayValues[2]) - 'A' + 'a');
          }
          else
          {
            //save the complementary pais in BP mode...
            changeukw(lookupukw(MenuData.UKWSocket) - 1, MenuData.UKWSocket + 'a' - 1); // before changing current pair, save (A-F) to (F-A)
          }
          MenuData.UKWPlugChanged = 0;

          MenuData.StayInThisMenu = checkUKWD();

          if (MenuData.StayInThisMenu)
          {
            // if uwk is still wrong, stay here
            break;
          }

          //otherwise, fix variables and fall through
          MenuData.Menu++;
          MenuData.SubChange = 0;
        }
      }

    case 3:
      {
        if (MenuData.SubChange == 0)
        {
          ShowMenu(F("ROTR"));
        }
        else
        {
          if (MenuData.SubChange == 2)
          {
            byte index = 0;
            byte command = 0;
            byte val = 0;

            //3210  index
            //1234  down keys
            //abcd  up keys

            switch (key)
            {
              // increment
              case 'a':
                {
                  index = 3;
                  command = 1;
                  break;
                }
              case 'b':
                {
                  index = 2;
                  command = 1;
                  break;
                }
              case 'c':
                {
                  index = 1;
                  command = 1;
                  break;
                }
              case 'd':
                {
                  index = 0;
                  command = 1;
                  break;
                }

              // decrement
              case '1':
                {
                  index = 3;
                  command = 2;
                  break;
                }
              case '2':
                {
                  index = 2;
                  command = 2;
                  break;
                }
              case '3':
                {
                  index = 1;
                  command = 2;
                  break;
                }
              case '4':
                {
                  index = 0;
                  command = 2;
                  break;
                }
            }

            val = GetRotorType(index);

            if ((command == 1) && (val != 0))
            {
              // handle Beta and Gamma greek wheels
              if ((GetMachineType() == 3) && (index == 3))
              {
                val = (val == 10) ? 9 : 10;
              }
              else
              {
                val = (val < GetMaxWheels()) ? val + 1 : 1;
              }
              SetRotorType(index, val);
            }

            if ((command == 2) && (val != 0))
            {
              // handle Beta and Gamma greek wheels
              if ((GetMachineType() == 3) && (index == 3))
              {
                val = (val == 9) ? 10 : 9;
              }
              else
              {
                val = (val > 1) ? val - 1 : GetMaxWheels();
              }
              SetRotorType(index, val);
            }

            UpdateWheelData();
          }

          byte RotorCheck[11]; // one more than the wheels for an M4 (1..10)

          for (byte i = 0; i < 11; i++)
          {
            RotorCheck[i] = 0;
          }

          for (byte i = 0; i < 4; i++)
          {
            char n;
            char d;

            ++RotorCheck[GetRotorType(i)];

            n = GetRotorName(3 - i);

            //the order of questions is important (0..9 A..Z a..z)
            if (n == '0')
            {
              d = 0;
            }
            else if (n > 'A' - 1)
            {
              d = n - 'A' + 1;
            }
            else if (n > '0')
            {
              d = n - '0' + 27;
            }

            NewDisplayValues[i] = d; // i:0123 -> RotorIndex:3210
            DisplayValues[i] = d;
          }

          MenuData.StayInThisMenu = 0;

          // disregard 0, as it is not a valid rotor
          for (byte i = 1; i < 11; i++)
          {
            if (RotorCheck[i] > 1)
            {
              MenuData.StayInThisMenu = 1;
            }
          }

          updateRawValues();
        }

        break;
      }

    case 4:
      {
        if ((GetNumberOfWheels() == 3) && (MenuData.LastSelectedRotor == 0))
        {
          MenuData.LastSelectedRotor = 1;
        }

        MenuData.LastRingIndex = 3 - MenuData.LastSelectedRotor;

        if ((MenuData.SubChange == 1) && (key > 'A' - 1) && (key < 'Z' + 1))
        {
          MenuData.SubChange = 2;
        }

        if (MenuData.SubChange == 0)
        {
          ShowMenu(F("RING"));
          MenuData.LastRingCommand = 0;
        }
        else
        {
          if (MenuData.SubChange == 2)
          {
            MenuData.LastRingCommand = 0;

            //3210  index
            //1234  down keys
            //abcd  up keys

            switch (key)
            {
              // increment
              case 'a':
                {
                  MenuData.LastRingIndex = 3;
                  MenuData.LastRingCommand = 1;
                  break;
                }
              case 'b':
                {
                  MenuData.LastRingIndex = 2;
                  MenuData.LastRingCommand = 1;
                  break;
                }
              case 'c':
                {
                  MenuData.LastRingIndex = 1;
                  MenuData.LastRingCommand = 1;
                  break;
                }
              case 'd':
                {
                  MenuData.LastRingIndex = 0;
                  MenuData.LastRingCommand = 1;
                  break;
                }

              // decrement
              case '1':
                {
                  MenuData.LastRingIndex = 3;
                  MenuData.LastRingCommand = 2;
                  break;
                }
              case '2':
                {
                  MenuData.LastRingIndex = 2;
                  MenuData.LastRingCommand = 2;
                  break;
                }
              case '3':
                {
                  MenuData.LastRingIndex = 1;
                  MenuData.LastRingCommand = 2;
                  break;
                }
              case '4':
                {
                  MenuData.LastRingIndex = 0;
                  MenuData.LastRingCommand = 2;
                  break;
                }
            }

            byte val;

            val = GetRingSetting(MenuData.LastRingIndex);

            if (MenuData.LastRingCommand == 1)
            {
              val = (val < 26) ? val + 1 : 1;
              SetRingSetting(MenuData.LastRingIndex, val);
            }

            if (MenuData.LastRingCommand == 2)
            {
              val = (val > 1) ? val - 1 : 26;
              SetRingSetting(MenuData.LastRingIndex, val);
            }

            if ((MenuData.LastRingCommand == 0) && (key > 'A' - 1) && (key < 'Z' + 1))
            {
              SetRingSetting(MenuData.LastRingIndex, key - ('A' - 1));

              MenuData.LastSelectedRotor++; // this gets copied to LastRingIndex next time we enter here

              if (MenuData.LastSelectedRotor == 4)
              {
                MenuData.LastSelectedRotor = 0;
              }
            }

            UpdateWheelData();
          }

          for (byte i = 0; i < 4; i++)
          {
            NewDisplayValues[i] = GetRingSetting(3 - i); // i:0123 -> RotorIndex:3210
            DisplayValues[i] = NewDisplayValues[i];
          }

          //BUG: make sure M4 and N machines show 4 rings
          if (GetNumberOfWheels() == 3)
          {
            NewDisplayValues[0] = 0;
            DisplayValues[0] = 0;
          }

          updateRawValues();
        }
        break;
      }

    case 5:
      {
        if (HasPlugboard(GetMachineType()))
        {
          if (MenuData.SubChange == 0)
          {
            if (key < 'A') // prevent blinking is keyboard is pressed while in this menu
            {
              Display4(F("PLUG"), false);
              allLightsOff();
              if (CheckPlugPairs())
              {
                ShowPlugs(0);
              }
              else
              {
                ShowPlugs(1);
              }

              MenuData.CurrentSWPLug = 1;
              MenuData.SWPlugLeft = GetPlugPair(0, 0);
              MenuData.SWPlugRight = GetPlugPair(0, 1);
              MenuData.SWPairChanged = 0;
              MenuData.SWPlugChanged = 0;
              MenuData.LastPlugLR = 0;
            }
          }
          else
          {
            // BUG: need to turn lights off upon entry here if a software plug is installed

            if ((key > 'A' - 1) && (key < 'Z' + 1))
            {
              MenuData.SubChange = 2;
            }

            if (MenuData.SubChange == 2)
            {
              // BUG: need to change plugs to PlugPairs[] (the order matters for the UHR)

              //3210  index
              //1234  down keys
              //abcd  up keys

              if ((key > 'A' - 1) && (key < 'Z' + 1))
              {
                MenuData.StayInThisMenu = 1;
                MenuData.SWPlugChanged = 1;

                ShowPlugs(0);
                allLightsOff();

                if (MenuData.LastPlugLR == 0)
                {
                  MenuData.LastPlugLR = 1;
                  MenuData.SWPlugLeft = key - ('A' - 1);
                }
                else
                {
                  MenuData.LastPlugLR = 0;
                  MenuData.SWPlugRight = key - ('A' - 1);
                }
              }

              switch (key)
              {
                // increment
                case 'a':
                case 'b':
                  {
                    MenuData.SWPairChanged = MenuData.CurrentSWPLug;
                    MenuData.LastPlugLR = 0;

                    MenuData.CurrentSWPLug++;
                    if (MenuData.CurrentSWPLug == 14)
                    {
                      MenuData.CurrentSWPLug = 1;
                    }
                    break;
                  }
                case 'c':
                  {
                    MenuData.StayInThisMenu = 1;
                    MenuData.SWPlugChanged = 1;
                    MenuData.LastPlugLR = 0;

                    ShowPlugs(0);
                    allLightsOff();

                    MenuData.SWPlugLeft++;
                    if (MenuData.SWPlugLeft > 26)
                    {
                      MenuData.SWPlugLeft = 0;
                    }
                    break;
                  }
                case 'd':
                  {
                    MenuData.StayInThisMenu = 1;
                    MenuData.SWPlugChanged = 1;
                    MenuData.LastPlugLR = 1;

                    ShowPlugs(0);
                    allLightsOff();

                    MenuData.SWPlugRight++;
                    if (MenuData.SWPlugRight > 26)
                    {
                      MenuData.SWPlugRight = 0;
                    }
                    break;
                  }

                // decrement
                case '1':
                case '2':
                  {
                    MenuData.SWPairChanged = MenuData.CurrentSWPLug;
                    MenuData.LastPlugLR = 0;

                    MenuData.CurrentSWPLug--;
                    if (MenuData.CurrentSWPLug == 0)
                    {
                      MenuData.CurrentSWPLug = 13;
                    }
                    break;
                    break;
                  }
                case '3':
                  {
                    MenuData.StayInThisMenu = 1;
                    MenuData.SWPlugChanged = 1;
                    MenuData.LastPlugLR = 0;

                    ShowPlugs(0);
                    allLightsOff();

                    MenuData.SWPlugLeft--;
                    if (MenuData.SWPlugLeft == 255)
                    {
                      MenuData.SWPlugLeft = 26;
                    }
                    break;
                  }
                case '4':
                  {
                    MenuData.StayInThisMenu = 1;
                    MenuData.SWPlugChanged = 1;
                    MenuData.LastPlugLR = 1;

                    ShowPlugs(0);
                    allLightsOff();

                    MenuData.SWPlugRight--;
                    if (MenuData.SWPlugRight == 255)
                    {
                      MenuData.SWPlugRight = 26;
                    }
                    break;
                  }
              }
            }

            //Serial.print("current:");
            //Serial.println(MenuData.CurrentSWPLug); //BUG: remove
            //Serial.print("pair:");
            //Serial.println(MenuData.SWPairChanged);
            //Serial.print("plug:");
            //Serial.println(MenuData.SWPlugChanged);

            if ((MenuData.SWPairChanged) || (key == '5'))
            {
              if (key == '5')
              {
                MenuData.SWPairChanged = MenuData.CurrentSWPLug;
              }

              if ((MenuData.SWPlugLeft == 0) || (MenuData.SWPlugRight == 0))
              {
                MenuData.SWPlugLeft = 0;
                MenuData.SWPlugRight = 0;
              }

              if (MenuData.SWPlugChanged)
              {
                //Serial.println("saved"); //BUG: remove

                SetPlugPair(MenuData.SWPairChanged - 1, 0, MenuData.SWPlugLeft);
                SetPlugPair(MenuData.SWPairChanged - 1, 1, MenuData.SWPlugRight);
                MenuData.SWPlugChanged = 0;
              }

              MenuData.SWPlugLeft = GetPlugPair(MenuData.CurrentSWPLug - 1, 0);
              MenuData.SWPlugRight = GetPlugPair(MenuData.CurrentSWPLug - 1, 1);

              MenuData.SWPairChanged = 0;
            }

            NewDisplayValues[1] = 27 + (MenuData.CurrentSWPLug % 10);
            if (MenuData.CurrentSWPLug > 9)
            {
              NewDisplayValues[0] = 28;
            }
            else
            {
              NewDisplayValues[0] = 0;
            }

            //BUG: remove
            //Serial.print("plugs: ");
            //Serial.print(MenuData.SWPlugLeft);
            //Serial.print(' ');
            //Serial.println(MenuData.SWPlugRight);

            NewDisplayValues[2] = MenuData.SWPlugLeft;
            NewDisplayValues[3] = MenuData.SWPlugRight;

            DisplayValues[0] = NewDisplayValues[0];
            DisplayValues[1] = NewDisplayValues[1];
            DisplayValues[2] = NewDisplayValues[2];
            DisplayValues[3] = NewDisplayValues[3];
            updateRawValues();
          }

          //Serial.println(MenuData.StayInThisMenu); //BUG: remove

          if ((MenuData.StayInThisMenu != 0) && (key == '5'))
          {
            //Serial.print("time to check "); //BUG: remove

            byte pairs = CheckPlugPairs();

            //Serial.println(pairs); //BUG: remove

            if (pairs == 99) // are plugs good? no repeats?
            {
              break; // stay here
            }
            else
            {
              EnableSWPlugs(pairs);

              MenuData.StayInThisMenu = 0;
              MenuData.Menu++;
              MenuData.SubChange = 0;
            }
          }
          else
          {
            break; // put break here to stay in menu 4, otherwise fall through to 5
          }
        }
        else
        {
          MenuData.Menu++;
          // fall through to case 6, no break;
        }
      }

    case 6:
      {
        if (HasPlugboard(GetMachineType()) && UHRPairsOK())
        {
          if (MenuData.SubChange == 0)
          {
            ShowMenu(F("-UHR"));
          }
          else
          {
            byte UHR = GetUHR();

            if (MenuData.SubChange == 2)
            {
              //3210  index
              //1234  down keys
              //abcd  up keys

              switch (key)
              {
                // increment
                case 'a':
                case 'b':
                case 'c':
                case 'd':
                  {
                    UHR++;
                    if (UHR == 40)
                    {
                      UHR = 0;
                    }
                    SetUHR(UHR);
                    ActivateUHR();
                    break;
                  }

                case '1':
                case '2':
                case '3':
                case '4':
                  {
                    UHR--;
                    if (UHR == 255)
                    {
                      UHR = 39;
                    }
                    SetUHR(UHR);
                    ActivateUHR();
                    break;
                  }
              }
            }

            NewDisplayValues[0] = 0;
            NewDisplayValues[1] = 0;

            NewDisplayValues[2] = 27 + UHR / 10;
            NewDisplayValues[3] = 27 + UHR % 10;

            DisplayValues[0] = NewDisplayValues[0];
            DisplayValues[1] = NewDisplayValues[1];
            DisplayValues[2] = NewDisplayValues[2];
            DisplayValues[3] = NewDisplayValues[3];
            updateRawValues();
          }

          break;
        }
        else
        {
          MenuData.Menu++;
          // fall through to case 7, no break;
        }
      }

    case 7:
      {
        ShowPlugs(0); // turn off plug visualization
        if (MenuData.SubChange == 0)
        {
          //the extra characters at the end are not shown by Display4, but processed by LampFieldMessage
          ShowMenu(F("*V17ER")); // displays *VER
        }
        else
        {
          LampsAndKeysSelfTest();
          exitmenu = 1;
        }

        break;
      }

    default:
      {
        exitmenu = 1;
        break;
      }
  }

  return exitmenu;
}


//
void SetMenuTimer()     /*xls*/
{
  MenuData.InhibitMenuTimer = 1500;
}


//
void DecMenuTimer()     /*xls*/
{
  if (MenuData.InhibitMenuTimer)
  {
    MenuData.InhibitMenuTimer--;
  }
}


//BUG: on a settable reflector machine, pushing up on A blanks the display
//BUG: save wheel content when entering, restore when exiting, changing items in rotor menu corrupts 3 wheel machine
//BUG: first level menu, submenu to be done as a separate subroutine (machine, rotor, ring)
//
void doAction(byte key, bool reset = false)     /*xls*/
{
  static byte state = 0;

  byte exitmenu;

  // to reset the menu state machine on long menu press
  if (reset)
  {
    state = 0;
    MenuData.Menu = 0;
    MenuData.StayInThisMenu = 0;
  }

  switch (state)
  {
    case 0:
      {
        if (key == '5')
        {
          if (MenuData.InhibitMenuTimer == 0) // when exiting menu, disable menu button for a little bit, prevents reentering menu.
          {
            // exit to menu
            stopScroll();
            MenuData.InhibitKeyboard = 1; //disable keyboard so pressing a key when inside the menu wont encode it
            MenuData.Menu = 1;
            MenuData.SubChange = 0;
            state = 1;
          }
        }
        else
        {
          MenuData.InhibitKeyboard = 0;
          ChangeRotor(key);
        }
        break;
      }

    case 1:
      {
        switch (key)
        {
          case '5':
            {
              // if something is wrong in a submenu, force user to stay there
              if (MenuData.StayInThisMenu == 0)
              {
                MenuData.Menu++;
                MenuData.SubChange = 0;
              }

              break;
            }

          case 'a':
          case 'b':
          case 'c':
          case 'd':
            {
              MenuData.SubChange++;
              if (MenuData.SubChange > 1)
              {
                MenuData.SubMenu[MenuData.Menu - 1]++;
              }
              break;
            }

          case '1':
          case '2':
          case '3':
          case '4':
            {
              MenuData.SubChange++;
              if (MenuData.SubChange > 1)
              {
                MenuData.SubMenu[MenuData.Menu - 1]--;
              }
              break;
            }
        }

        break;
      }
  }

  if (MenuData.Menu)
  {
    if (MenuData.SubChange == 2)
    {
      MenuData.RotorsChanged = 1;
    }

    if (MenuData.SubChange > 2)
    {
      MenuData.SubChange = 2;
    }

    exitmenu = doMenu(key);

    if (exitmenu)
    {
      allLightsOff();

      if (SaveEnigmaToEEPROM() == 0)
      {
        SetMenuTimer();
      }

      ShowRotorPositions();
      MenuData.InhibitKeyboard = 0; // exiting menu, reenable keyboard
      MenuData.Menu = 0;
      state = 0;
    }
  }
}


// BUG: press a key, try changing the right rotor, press menu, exit menu, right rotor can be changed (MenuData.KeyPressed = 0)
// BUG: check IsWheelLockedDec logic for middle wheels
//
void ChangeRotor(byte key)     /*xls*/
{
  byte thumbwheelused = 0;

  EnigmaCheckWheelLocked();

  switch (key)
  {
    case '1':
      if ((MenuData.KeyPressed == 0) || (IsWheelLockedDec(3) == 0))
      {
        //if pushed in the middle of a transition, finish it
        if (NewDisplayValues[0] != DisplayValues[0])
        {
          DisplayValues[0] = NewDisplayValues[0];
        }
        //then apply the normal logic
        NewDisplayValues[0] = DisplayValues[0] - 1;
        thumbwheelused = 1;
      }
      break;
    case 'a':
      if ((MenuData.KeyPressed == 0) || (IsWheelLockedInc(3) == 0))
      {
        if (NewDisplayValues[0] != DisplayValues[0])
        {
          DisplayValues[0] = NewDisplayValues[0];
        }
        NewDisplayValues[0] = DisplayValues[0] + 1;
        thumbwheelused = 1;
      }
      break;
    case '2':
      if ((MenuData.KeyPressed == 0) || (IsWheelLockedDec(2) == 0))
      {
        if (NewDisplayValues[1] != DisplayValues[1])
        {
          DisplayValues[1] = NewDisplayValues[1];
        }
        NewDisplayValues[1] = DisplayValues[1] - 1;
        thumbwheelused = 1;
      }
      break;
    case 'b':
      if ((MenuData.KeyPressed == 0) || (IsWheelLockedInc(2) == 0))
      {
        if (NewDisplayValues[1] != DisplayValues[1])
        {
          DisplayValues[1] = NewDisplayValues[1];
        }
        NewDisplayValues[1] = DisplayValues[1] + 1;
        thumbwheelused = 1;
      }
      break;
    case '3':
      if ((MenuData.KeyPressed == 0) || (IsWheelLockedDec(1) == 0))
      {
        if (NewDisplayValues[2] != DisplayValues[2])
        {
          DisplayValues[2] = NewDisplayValues[2];
        }
        NewDisplayValues[2] = DisplayValues[2] - 1;
        thumbwheelused = 1;
      }
      break;
    case 'c':
      if ((MenuData.KeyPressed == 0) || (IsWheelLockedInc(1) == 0))
      {
        if (NewDisplayValues[2] != DisplayValues[2])
        {
          DisplayValues[2] = NewDisplayValues[2];
        }
        NewDisplayValues[2] = DisplayValues[2] + 1;
        thumbwheelused = 1;
      }
      break;
    case '4':
      // once a key is pressed, the stepping pawl locks the fast wheel
      if ((MenuData.KeyPressed == 0) || (IsWheelLockedDec(0) == 0))
      {
        if (NewDisplayValues[3] != DisplayValues[3])
        {
          DisplayValues[3] = NewDisplayValues[3];
        }
        NewDisplayValues[3] = DisplayValues[3] - 1;
        thumbwheelused = 1;
      }
      break;
    case 'd':
      // once a key is pressed, the stepping pawl locks the fast wheel
      if ((MenuData.KeyPressed == 0) || (IsWheelLockedInc(0) == 0))
      {
        if (NewDisplayValues[3] != DisplayValues[3])
        {
          DisplayValues[3] = NewDisplayValues[3];
        }
        NewDisplayValues[3] = DisplayValues[3] + 1;
        thumbwheelused = 1;
      }
      break;
  }

  if (thumbwheelused == 1)
  {
    //BUG: on a three wheel machine, pushing up and pressing A shows a rotor (FIXED)

    MenuData.RotorsChanged = 1;

    if ((GetRotorType(3) == 0) && (GetSettableUWK() == 0))
    {
      NewDisplayValues[0] = 0;
      DisplayValues[0] = 0;
    }

    for (byte i = 0; i < 4; i++)
    {
      if ((GetRotorType(3 - i) != 0) || ((i == 0) && GetSettableUWK()))
      {
        if (NewDisplayValues[i] == 0) // BUG: use GetRotorType to keep a blank wheel blank
        {
          NewDisplayValues[i] = 26;
        }
        // order is important, check this here as 255 is > 9
        if (NewDisplayValues[i] > 26)
        {
          NewDisplayValues[i] = 1;
        }

        SetRotorPosition(3 - i, NewDisplayValues[i] + 'A' - 1); // i:0123 -> RotorIndex:3210
      }
    }
  }
}


//
void StepAndUpdate()     /*xls*/
{
  EnigmaStep();
  for (byte i = 0; i < 4; i++)
  {
    NewDisplayValues[i] = GetRotorPosition(3 - i) - 'A' + 1;
  }
}


//
void DoEnigma()     /*xls*/
{
  static byte allKeysUpPrev = 1;
  byte allKeysUpNow = 1;
  byte thumbWheels = 0;

  //entrytimer(TASKDUMMYTASK);

  for (byte i = 0; i < 26; i++)
  {
    if (isPressed(i))
    {
      allKeysUpNow = 0;
    }
  }

  for (byte i = 26; i < 35; i++)
  {
    if (isPressed(i))
    {
      thumbWheels = 1;
    }
  }

  //exittimer(TASKDUMMYTASK);

  if ((allKeysUpNow == 0) && (allKeysUpPrev == 1) && (MenuData.InhibitKeyboard == 0) && (thumbWheels == 0))
  {
    StepAndUpdate();
    MenuData.KeyPressed = 1;
  }

  allKeysUpPrev = allKeysUpNow;
}


void ProcessEnigma()     /*xls*/
{
  static byte state = 0;
  static byte keyindex = 0;

  static byte anypressed = 1;
  static byte lampon = 0;

  static byte enigmain = 0;
  static byte serialin = 0;
  static byte out = 0;

  static byte serialsetpos = 0;
  static byte serialposread = 0;
  static byte serialpos[4];

  static byte groupcnt = 0;
  static byte groupscnt = 0;
  static byte startnewgroup = 0;

  static int  waitcount = 0;

  //for multi lamp  on external lampfield
  static byte lampA = 0;
  static byte lampB = 0;

  char k;

  if (GetNeedsSaving())
  {
    startnewgroup = 1;
    groupcnt = 1;
  }

  if (MenuData.InhibitKeyboard)
  {
    state = 0;
    serialsetpos = 0;
    for (byte i = 0; i < 26; i++)
    {
      MenuData.EnigmaResults[i] = 0;
    }
    lampA = 0;
    lampB = 0;
  }

  if (LampfieldEnabled() == 0) // this logic works best
  {
    allLightsOff();
    for (byte i = 0; i < 26; i++)
    {
      MenuData.EnigmaResults[i] = 0;
    }
    lampA = 0;
    lampB = 0;
  }

  switch (state)
  {
    case 0:
      {
        k = Serial.read(); // put outside of inhibitkeyboard check to keep buffer clean

        if (MenuData.InhibitKeyboard == 0)
        {
          if (k == '~')
          {
            allLightsOff();
          }

          if (k == '!')
          {
            startnewgroup = 1;
            serialsetpos = 1;
            serialpos[0] = 0;
            serialposread = 4 - GetNumberOfWheels();
          }

          k = k & (255 - 32);

          if ((k > ('A' - 1)) && (k < ('Z' + 1)) && (MenuData.InhibitKeyboard == 0))
          {
            if (serialsetpos == 0)
            {
              if (startnewgroup)
              {
                startnewgroup = 0;
                if ((groupcnt) || (groupscnt))
                {
                  groupcnt = 0;
                  groupscnt = 0;
                  Serial.print(F("\x0d\x0a")); // memory efficient way to send a newline
                }
              }

              serialsetpos = 0;
              enigmain = k - 'A' + 1;
              serialin = 1;
              EnigmaEncode(255);
              StepAndUpdate();
              EnigmaEncode(enigmain);
              state = 1;
            }
            else
            {
              serialpos[serialposread] = k;
              serialposread++;

              if (serialposread == 4)
              {
                serialsetpos = 0;
                serialposread = 0;
                for (byte i = 0; i < 4; i++)
                {
                  byte v = serialpos[i];

                  //DisplayValues[i] = v - 'A' + 1;     // if uncommented, change is not animated
                  NewDisplayValues[i] = v - 'A' + 1;
                  SetRotorPosition(3 - i, v);
                }
                updateRawValues();
                MenuData.RotorsChanged = 1;
              }
            }
          }

          if (isPressed(keyindex))
          {
            serialin = 0;
            anypressed = 0;
            enigmain = keyindex + 1;
            EnigmaEncode(255);
            EnigmaEncode(enigmain);
            MenuData.RotorsChanged = 1;
            state = 1;
          }
          else
          {
            lightOff(MenuData.EnigmaResults[keyindex]); // 0 means no result, disregarded by lightOff

            byte restorelamp = 0;

            if ((lampA != 0) && (lampB != 0))
            {
              if (MenuData.EnigmaResults[keyindex] == lampA)
              {
                restorelamp = lampB;
                lampA = lampB;
                lampB = 0;
              }

              if (MenuData.EnigmaResults[keyindex] == lampB)
              {
                restorelamp = lampA;
                lampB = 0;
              }
            }

            if (restorelamp)
            {
              Serial.print(F("\x0d\x0a"));
              Serial.print(F("er>er>")); // send sequence to turn on external lampfield
              Serial.print((char)('A' + restorelamp - 1));
              Serial.print(F("    "));
            }

            MenuData.EnigmaResults[keyindex] = 0;
            keyindex++;
            if (keyindex > 25)
            {
              if ((anypressed) && (lampon))
              {
                lampon = 0;
                Serial.print(F("\x0d\x0a")); // turn off external lampfield (send a newline)
                lampA = 0;
                lampB = 0;
              }

              MenuData.KeyPressed = anypressed ^ 1;

              keyindex = 0;
              anypressed = 1;
            }
          }
        }

        break;
      }

    case 1:
      {
        if (GetRotorsChanged())
        {
          EnigmaEncode(255);
          EnigmaEncode(enigmain);
          lampA = 0;
          lampB = 0;
        }

        out = EnigmaEncode(0);

        if (out != 255)
        {
          if (serialin)
          {
            Serial.print((char)('A' + out - 1));

            groupcnt++;
            if (groupcnt >= GetPrintGroups())
            {
              Serial.print(F(" "));
              groupcnt = 0;
              groupscnt++;
              if (groupscnt > 7)
              {
                Serial.print(F("\x0d\x0a"));
                groupscnt = 0;
              }
            }

            serialin = 0;
            state = 99;
          }
          else
          {
            state = 2;
          }
        }

        break;
      }

    case 2:
      {
        if (GetRotorsChanged())
        {
          EnigmaEncode(255);
          EnigmaEncode(enigmain);
          lampA = 0;
          lampB = 0;
          state = 1;
        }

        byte nextstate = 5;

        if (LampfieldEnabled())
        {
          byte old = MenuData.EnigmaResults[keyindex];

          if ((old != out) && (old != 255))
          {
            lightOff(old);

            if (lampA == 0)
            {
              lampA = out;
            }
            else
            {
              lampB = out;
            }

            if (isPressed(out - 1) == 0)
            {
              Serial.print(F("\x0d\x0a"));
              Serial.print(F("er>er>")); // send sequence to turn on external lampfield
              Serial.print((char)('A' + out - 1));
              nextstate = 3;
            }
            else
            {
              Serial.print(F("\x0d\x0a"));
            }
          }

          if (isPressed(out - 1) == 0)
          {
            if (old == 255)
            {
              Serial.print(F("\x0d\x0a"));
              Serial.print(F("er>er>")); // send sequence to turn on external lampfield
              Serial.print((char)('A' + out - 1));
              nextstate = 3;
            }

            lampon = 1;
          }
          else
          {
            lightOff(out);
            lightOff(keyindex + 1);

            out = 255;
          }

          waitcount = 100;

          MenuData.EnigmaResults[keyindex] = out;
        }

        state = nextstate;
        break;
      }

    case 3:
      {
        if (GetRotorsChanged())
        {
          EnigmaEncode(255);
          EnigmaEncode(enigmain);
          lampA = 0;
          lampB = 0;
          state = 1;
        }

        k = Serial.read();

        if (k == '~')
        {
          allLightsOff();
          state = 5;
        }

        if (--waitcount == 0)
        {
          state = 4;
        }

        break;
      }

    case 4:
      {
        // if key is released before getting here, light stays off
        if (isPressed(keyindex))
        {
          lightOn(out);
        }
        state = 5;

        break;
      }

    default:
      {
        //return to state 0 and advance to the next key
        state = 0;
        keyindex++;
        if (keyindex > 25)
        {
          keyindex = 0;
        }

        break;
      }
  }
}
