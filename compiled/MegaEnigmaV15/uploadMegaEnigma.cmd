@echo off
set enigmaname=MegaEnigmaV15

set backupname=BackupPrior-%enigmaname%-Port%1%.hex

if not "%1"=="" goto process 

echo.
echo usage:
echo.
echo to upgrade the software in your simulator:
echo.
echo %0 NN
echo.
echo to undo this upgrade and restore the previous software loaded in your simulator:
echo.
echo %0 NN r
echo.
echo where NN is the numeric part of the COM ports listed below
echo for example, for COM4 type:
echo.
echo %0 4
echo.
echo list of ports:
echo.
\Windows\system32\mode | \Windows\system32\find "COM"
echo.

goto :exit

:process

if "%2"=="r" goto restorebackup 
if "%2"=="R" goto restorebackup 

set backupcount=

set avrpath=%PROGRAMFILES%\Arduino\hardware\tools\avr\
if exist "%avrpath%bin\avrdude.exe" goto startbackup

set avrpath=%PROGRAMFILES(x86)%\Arduino\hardware\tools\avr\
if exist "%avrpath%bin\avrdude.exe" goto startbackup

echo.
echo Cannot find AVRDUDE.EXE, ensure Arduino IDE is installed
echo If the Arduino IDE is installed, copy this folder
echo to the same drive i.e: C:\ or D:\ that contains your Arduino installation
echo.

goto :exit

:startbackup

echo.

if "%2"=="-NB" goto :skipbackup

if exist "%backupname%" goto :skipbackup

echo %0: Backing up prior version.

"%avrpath%bin\avrdude" -C"%avrpath%etc\avrdude.conf" -v -p atmega2560 -c wiring -P COM%1 -b 115200 -D -Uflash:r:"%backupname%":i

if exist "%backupname%" goto :skipbackup

echo %0: Backup failed, trying again, to skip backup use: %0 %1 -NB 

set backupcount=%backupcount%1

if "%backupcount%"=="111" goto :exit

goto :startbackup

:skipbackup

echo %0: Loading new version.

"%avrpath%bin\avrdude" -C"%avrpath%etc\avrdude.conf" -v -p atmega2560 -c wiring -P COM%1 -b 115200 -D -Uflash:w:"%enigmaname%.hex":i

goto :exit

:restorebackup

if exist "%backupname%" goto :dorestore

echo.
echo Backup file does not exist, exiting... 
echo.

goto :exit

:dorestore

echo.
echo %0: Restoring previous version.

"%avrpath%bin\avrdude" -C"%avrpath%etc\avrdude.conf" -v -p atmega2560 -c wiring -P COM%1 -b 115200 -D -Uflash:w:"%backupname%":i

:exit

echo %0: Done
